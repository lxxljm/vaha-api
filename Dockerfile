FROM 113.108.106.175:444/cmp/java:11

ENV TZ=Asia/Shanghai

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD target/vaha-0.0.1.jar /home

WORKDIR /home

ENTRYPOINT ["java", "-jar", "vaha-0.0.1.jar"]
