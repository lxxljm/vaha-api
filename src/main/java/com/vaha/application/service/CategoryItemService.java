package com.vaha.application.service;

import com.cyber.application.service.BaseService;

import com.vaha.domain.entity.CategoryItem;

import java.util.List;

public interface CategoryItemService extends BaseService<CategoryItem> {


    List<CategoryItem> selectByCodes(List<String> categoryCodes);
}
