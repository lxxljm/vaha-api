package com.vaha.application.service;

import com.cyber.application.service.BaseService;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.entity.Category;

public interface CategoryService extends BaseService<Category> {


}
