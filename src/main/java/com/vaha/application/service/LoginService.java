package com.vaha.application.service;


import com.cyber.domain.entity.AuthingToken;
import me.zhyd.oauth.model.AuthUser;

/**
 * Created by shiye on 2023/7/15.
 */
public interface LoginService<T> {

    AuthingToken<T> login(String username, String password);

    AuthingToken<T> thirdAuthingCheckAccount(AuthUser user, String source);

    void logout(String userId, String loginName);
}
