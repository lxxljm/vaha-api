package com.vaha.application.service;

import com.vaha.domain.entity.UserAccount;

/**
 * Created by shiye on 2023/7/14.
 */
public interface PasswordService {
    void validate(UserAccount userAccount, String password);
}
