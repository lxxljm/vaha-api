package com.vaha.application.service;

import com.cyber.application.service.BaseService;

import com.vaha.domain.entity.ProductFollow;

public interface ProductFollowService extends BaseService<ProductFollow> {


    boolean checkProductFollowUnique(ProductFollow productfollow);
}
