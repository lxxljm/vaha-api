package com.vaha.application.service;

import com.cyber.application.service.BaseService;

import com.vaha.domain.entity.ProductItem;

import java.util.List;

public interface ProductItemService extends BaseService<ProductItem> {


    List<ProductItem> selectByCodes(List<String> productItemCodes);

}
