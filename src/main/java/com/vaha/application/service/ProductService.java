package com.vaha.application.service;

import com.cyber.application.service.BaseService;

import com.vaha.domain.entity.Product;

import java.util.List;

public interface ProductService extends BaseService<Product>{


    List<Product> selectByCodes(List<String> productCodes);

}
