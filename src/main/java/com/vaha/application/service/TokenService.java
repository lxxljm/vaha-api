package com.vaha.application.service;


import com.cyber.domain.entity.AuthingToken;

import java.util.Map;

/**
 * token验证处理
 */
public interface TokenService<T> {

    /**
     * 创建令牌
     */
    Map<String, Object> createToken(AuthingToken<T> loginUser);


}
