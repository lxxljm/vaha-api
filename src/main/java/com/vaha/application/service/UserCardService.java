package com.vaha.application.service;

import com.cyber.application.service.BaseBulkService;
import com.cyber.application.service.BaseService;

import com.vaha.domain.entity.UserCard;

import java.util.List;

public interface UserCardService extends BaseService<UserCard>, BaseBulkService<UserCard> {


    List<UserCard> selectByCodes(List<String> userCardCodes);

}
