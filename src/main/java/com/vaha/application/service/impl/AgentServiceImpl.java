package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.AgentMapper;
import com.vaha.domain.entity.Agent;
import com.vaha.application.service.AgentService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class AgentServiceImpl implements AgentService {

    private final AgentMapper agentMapper;

    @Override
    @Transactional
    public Integer save(Agent agent) {

        if( null == agent ) {
            log.warn("save agent, but agent is null...");
            return 0;
        }

        return agentMapper.save( agent );
    }

    @Override
    @Transactional
    public Integer deleteById(Agent agent) {

        if( null == agent ) {
            log.warn("delete agent, but agent is null  or agent id is null...");
            return 0;
        }

        return agentMapper.deleteById( agent );
    }

    @Override
    @Transactional
    public Integer updateById(Agent agent) {

        if( null == agent ) {
            log.warn("update agent, but agent is null  or agent id is null...");
            return 0;
        }

        return agentMapper.updateById( agent );
    }

    @Override
    public Agent selectOne(Agent agent) {
        if( null == agent ) {
            log.warn("select agent one, but agent is null ...");
            return null;
        }
        agent = agentMapper.selectOne( agent );
        return agent;
    }


    @Override
    public PagingData<Agent> selectPage(Agent agent) {
        PagingData<Agent> pagingData = new PagingData<>();

        if( null == agent ) {
            log.warn("select agent page, but agent is null...");
            return pagingData;
        }

        Integer queryCount = agentMapper.selectByIndexCount( agent );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select agent page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<Agent> agents =  selectByIndex( agent );
        pagingData.setData( agents );
        return pagingData;
    }

    @Override
    public List<Agent> selectByIndex(Agent agent) {
        List<Agent> agents = new ArrayList<>();
        if( null == agent ) {
            log.warn("select agent by index, but agent is null ...");
            return agents;
        }

        agents = agentMapper.selectByIndex( agent );

        return agents;
    }
}
