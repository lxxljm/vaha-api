package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.CategoryItemMapper;
import com.vaha.domain.entity.CategoryItem;
import com.vaha.application.service.CategoryItemService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CategoryItemServiceImpl implements CategoryItemService {

    private final CategoryItemMapper categoryItemMapper;

    @Override
    @Transactional
    public Integer save(CategoryItem categoryItem) {

        if( null == categoryItem ) {
            log.warn("save categoryItem, but categoryItem is null...");
            return 0;
        }

        return categoryItemMapper.save( categoryItem );
    }

    @Override
    @Transactional
    public Integer deleteById(CategoryItem categoryItem) {

        if( null == categoryItem ) {
            log.warn("delete categoryItem, but categoryItem is null  or categoryItem id is null...");
            return 0;
        }

        return categoryItemMapper.deleteById( categoryItem );
    }

    @Override
    @Transactional
    public Integer updateById(CategoryItem categoryItem) {

        if( null == categoryItem ) {
            log.warn("update categoryItem, but categoryItem is null  or categoryItem id is null...");
            return 0;
        }

        return categoryItemMapper.updateById( categoryItem );
    }

    @Override
    public CategoryItem selectOne(CategoryItem categoryItem) {
        if( null == categoryItem ) {
            log.warn("select categoryItem one, but categoryItem is null ...");
            return null;
        }
        categoryItem = categoryItemMapper.selectOne( categoryItem );
        return categoryItem;
    }


    @Override
    public PagingData<CategoryItem> selectPage(CategoryItem categoryItem) {
        PagingData<CategoryItem> pagingData = new PagingData<>();

        if( null == categoryItem ) {
            log.warn("select categoryItem page, but categoryItem is null...");
            return pagingData;
        }

        Integer queryCount = categoryItemMapper.selectByIndexCount( categoryItem );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select categoryItem page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<CategoryItem> categoryItems =  selectByIndex( categoryItem );
        pagingData.setData( categoryItems );
        return pagingData;
    }

    @Override
    public List<CategoryItem> selectByIndex(CategoryItem categoryItem) {
        List<CategoryItem> categoryItems = new ArrayList<>();
        if( null == categoryItem ) {
            log.warn("select categoryItem by index, but categoryItem is null ...");
            return categoryItems;
        }

        categoryItems = categoryItemMapper.selectByIndex( categoryItem );

        return categoryItems;
    }

    @Override
    public List<CategoryItem> selectByCodes(List<String> categoryCodes) {

        return categoryItemMapper.selectByCodes(categoryCodes);
    }
}
