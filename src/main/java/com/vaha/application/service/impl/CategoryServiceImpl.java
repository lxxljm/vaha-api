package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.entity.CategoryItem;
import com.vaha.domain.repository.CategoryItemMapper;
import com.vaha.domain.repository.CategoryMapper;
import com.vaha.domain.entity.Category;
import com.vaha.application.service.CategoryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CategoryServiceImpl implements CategoryService {

    private final CategoryMapper categoryMapper;

    private final CategoryItemMapper categoryItemMapper;

    @Override
    @Transactional
    public Integer save(Category category) {

        if( null == category ) {
            log.warn("save category, but category is null...");
            return 0;
        }

        return categoryMapper.save( category );
    }

    @Override
    @Transactional
    public Integer deleteById(Category category) {

        if( null == category ) {
            log.warn("delete category, but category is null  or category id is null...");
            return 0;
        }

        return categoryMapper.deleteById( category );
    }

    @Override
    @Transactional
    public Integer updateById(Category category) {

        if( null == category ) {
            log.warn("update category, but category is null  or category id is null...");
            return 0;
        }

        return categoryMapper.updateById( category );
    }

    @Override
    public Category selectOne(Category category) {
        if( null == category ) {
            log.warn("select category one, but category is null ...");
            return null;
        }
        category = categoryMapper.selectOne( category );
        return category;
    }


    @Override
    public PagingData<Category> selectPage(Category category) {
        PagingData<Category> pagingData = new PagingData<>();

        if( null == category ) {
            log.warn("select category page, but category is null...");
            return pagingData;
        }

        Integer queryCount = categoryMapper.selectByIndexCount( category );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select category page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<Category> categorys =  selectByIndex( category );
        pagingData.setData( categorys );
        return pagingData;
    }

    @Override
    public List<Category> selectByIndex(Category category) {
        List<Category> categorys = new ArrayList<>();
        if( null == category ) {
            log.warn("select category by index, but category is null ...");
            return categorys;
        }

        categorys = categoryMapper.selectByIndex( category );

        return categorys;
    }
}
