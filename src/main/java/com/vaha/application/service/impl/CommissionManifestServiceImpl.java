package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.CommissionManifestMapper;
import com.vaha.domain.entity.CommissionManifest;
import com.vaha.application.service.CommissionManifestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CommissionManifestServiceImpl implements CommissionManifestService {

    private final CommissionManifestMapper commissionManifestMapper;

    @Override
    @Transactional
    public Integer save(CommissionManifest commissionManifest) {

        if( null == commissionManifest ) {
            log.warn("save commissionManifest, but commissionManifest is null...");
            return 0;
        }

        return commissionManifestMapper.save( commissionManifest );
    }

    @Override
    @Transactional
    public Integer deleteById(CommissionManifest commissionManifest) {

        if( null == commissionManifest ) {
            log.warn("delete commissionManifest, but commissionManifest is null  or commissionManifest id is null...");
            return 0;
        }

        return commissionManifestMapper.deleteById( commissionManifest );
    }

    @Override
    @Transactional
    public Integer updateById(CommissionManifest commissionManifest) {

        if( null == commissionManifest ) {
            log.warn("update commissionManifest, but commissionManifest is null  or commissionManifest id is null...");
            return 0;
        }

        return commissionManifestMapper.updateById( commissionManifest );
    }

    @Override
    public CommissionManifest selectOne(CommissionManifest commissionManifest) {
        if( null == commissionManifest ) {
            log.warn("select commissionManifest one, but commissionManifest is null ...");
            return null;
        }
        commissionManifest = commissionManifestMapper.selectOne( commissionManifest );
        return commissionManifest;
    }


    @Override
    public PagingData<CommissionManifest> selectPage(CommissionManifest commissionManifest) {
        PagingData<CommissionManifest> pagingData = new PagingData<>();

        if( null == commissionManifest ) {
            log.warn("select commissionManifest page, but commissionManifest is null...");
            return pagingData;
        }

        Integer queryCount = commissionManifestMapper.selectByIndexCount( commissionManifest );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select commissionManifest page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<CommissionManifest> commissionManifests =  selectByIndex( commissionManifest );
        pagingData.setData( commissionManifests );
        return pagingData;
    }

    @Override
    public List<CommissionManifest> selectByIndex(CommissionManifest commissionManifest) {
        List<CommissionManifest> commissionManifests = new ArrayList<>();
        if( null == commissionManifest ) {
            log.warn("select commissionManifest by index, but commissionManifest is null ...");
            return commissionManifests;
        }

        commissionManifests = commissionManifestMapper.selectByIndex( commissionManifest );

        return commissionManifests;
    }
}
