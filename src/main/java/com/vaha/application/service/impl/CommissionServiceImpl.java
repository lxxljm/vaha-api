package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.CommissionMapper;
import com.vaha.domain.entity.Commission;
import com.vaha.application.service.CommissionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CommissionServiceImpl implements CommissionService {

    private final CommissionMapper commissionMapper;

    @Override
    @Transactional
    public Integer save(Commission commission) {

        if( null == commission ) {
            log.warn("save commission, but commission is null...");
            return 0;
        }

        return commissionMapper.save( commission );
    }

    @Override
    @Transactional
    public Integer deleteById(Commission commission) {

        if( null == commission ) {
            log.warn("delete commission, but commission is null  or commission id is null...");
            return 0;
        }

        return commissionMapper.deleteById( commission );
    }

    @Override
    @Transactional
    public Integer updateById(Commission commission) {

        if( null == commission ) {
            log.warn("update commission, but commission is null  or commission id is null...");
            return 0;
        }

        return commissionMapper.updateById( commission );
    }

    @Override
    public Commission selectOne(Commission commission) {
        if( null == commission ) {
            log.warn("select commission one, but commission is null ...");
            return null;
        }
        commission = commissionMapper.selectOne( commission );
        return commission;
    }


    @Override
    public PagingData<Commission> selectPage(Commission commission) {
        PagingData<Commission> pagingData = new PagingData<>();

        if( null == commission ) {
            log.warn("select commission page, but commission is null...");
            return pagingData;
        }

        Integer queryCount = commissionMapper.selectByIndexCount( commission );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select commission page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<Commission> commissions =  selectByIndex( commission );
        pagingData.setData( commissions );
        return pagingData;
    }

    @Override
    public List<Commission> selectByIndex(Commission commission) {
        List<Commission> commissions = new ArrayList<>();
        if( null == commission ) {
            log.warn("select commission by index, but commission is null ...");
            return commissions;
        }

        commissions = commissionMapper.selectByIndex( commission );

        return commissions;
    }
}
