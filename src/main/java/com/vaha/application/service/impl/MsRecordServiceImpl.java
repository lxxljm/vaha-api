package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.MsRecordMapper;
import com.vaha.domain.entity.MsRecord;
import com.vaha.application.service.MsRecordService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MsRecordServiceImpl implements MsRecordService {

    private final MsRecordMapper msRecordMapper;

    @Override
    @Transactional
    public Integer save(MsRecord msRecord) {

        if( null == msRecord ) {
            log.warn("save msRecord, but msRecord is null...");
            return 0;
        }

        return msRecordMapper.save( msRecord );
    }

    @Override
    @Transactional
    public Integer deleteById(MsRecord msRecord) {

        if( null == msRecord ) {
            log.warn("delete msRecord, but msRecord is null  or msRecord id is null...");
            return 0;
        }

        return msRecordMapper.deleteById( msRecord );
    }

    @Override
    @Transactional
    public Integer updateById(MsRecord msRecord) {

        if( null == msRecord ) {
            log.warn("update msRecord, but msRecord is null  or msRecord id is null...");
            return 0;
        }

        return msRecordMapper.updateById( msRecord );
    }

    @Override
    public MsRecord selectOne(MsRecord msRecord) {
        if( null == msRecord ) {
            log.warn("select msRecord one, but msRecord is null ...");
            return null;
        }
        msRecord = msRecordMapper.selectOne( msRecord );
        return msRecord;
    }


    @Override
    public PagingData<MsRecord> selectPage(MsRecord msRecord) {
        PagingData<MsRecord> pagingData = new PagingData<>();

        if( null == msRecord ) {
            log.warn("select msRecord page, but msRecord is null...");
            return pagingData;
        }

        Integer queryCount = msRecordMapper.selectByIndexCount( msRecord );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select msRecord page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<MsRecord> msRecords =  selectByIndex( msRecord );
        pagingData.setData( msRecords );
        return pagingData;
    }

    @Override
    public List<MsRecord> selectByIndex(MsRecord msRecord) {
        List<MsRecord> msRecords = new ArrayList<>();
        if( null == msRecord ) {
            log.warn("select msRecord by index, but msRecord is null ...");
            return msRecords;
        }

        msRecords = msRecordMapper.selectByIndex( msRecord );

        return msRecords;
    }
}
