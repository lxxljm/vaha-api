package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.MsTempalteMapper;
import com.vaha.domain.entity.MsTempalte;
import com.vaha.application.service.MsTempalteService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MsTempalteServiceImpl implements MsTempalteService {

    private final MsTempalteMapper msTempalteMapper;

    @Override
    @Transactional
    public Integer save(MsTempalte msTempalte) {

        if( null == msTempalte ) {
            log.warn("save msTempalte, but msTempalte is null...");
            return 0;
        }

        return msTempalteMapper.save( msTempalte );
    }

    @Override
    @Transactional
    public Integer deleteById(MsTempalte msTempalte) {

        if( null == msTempalte ) {
            log.warn("delete msTempalte, but msTempalte is null  or msTempalte id is null...");
            return 0;
        }

        return msTempalteMapper.deleteById( msTempalte );
    }

    @Override
    @Transactional
    public Integer updateById(MsTempalte msTempalte) {

        if( null == msTempalte ) {
            log.warn("update msTempalte, but msTempalte is null  or msTempalte id is null...");
            return 0;
        }

        return msTempalteMapper.updateById( msTempalte );
    }

    @Override
    public MsTempalte selectOne(MsTempalte msTempalte) {
        if( null == msTempalte ) {
            log.warn("select msTempalte one, but msTempalte is null ...");
            return null;
        }
        msTempalte = msTempalteMapper.selectOne( msTempalte );
        return msTempalte;
    }


    @Override
    public PagingData<MsTempalte> selectPage(MsTempalte msTempalte) {
        PagingData<MsTempalte> pagingData = new PagingData<>();

        if( null == msTempalte ) {
            log.warn("select msTempalte page, but msTempalte is null...");
            return pagingData;
        }

        Integer queryCount = msTempalteMapper.selectByIndexCount( msTempalte );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select msTempalte page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<MsTempalte> msTempaltes =  selectByIndex( msTempalte );
        pagingData.setData( msTempaltes );
        return pagingData;
    }

    @Override
    public List<MsTempalte> selectByIndex(MsTempalte msTempalte) {
        List<MsTempalte> msTempaltes = new ArrayList<>();
        if( null == msTempalte ) {
            log.warn("select msTempalte by index, but msTempalte is null ...");
            return msTempaltes;
        }

        msTempaltes = msTempalteMapper.selectByIndex( msTempalte );

        return msTempaltes;
    }
}
