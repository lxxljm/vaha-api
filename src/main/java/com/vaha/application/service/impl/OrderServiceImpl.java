package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.OrderMapper;
import com.vaha.domain.entity.Order;
import com.vaha.application.service.OrderService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

    private final OrderMapper orderMapper;

    @Override
    @Transactional
    public Integer save(Order order) {

        if( null == order ) {
            log.warn("save order, but order is null...");
            return 0;
        }

        return orderMapper.save( order );
    }

    @Override
    @Transactional
    public Integer deleteById(Order order) {

        if( null == order ) {
            log.warn("delete order, but order is null  or order id is null...");
            return 0;
        }

        return orderMapper.deleteById( order );
    }

    @Override
    @Transactional
    public Integer updateById(Order order) {

        if( null == order ) {
            log.warn("update order, but order is null  or order id is null...");
            return 0;
        }

        return orderMapper.updateById( order );
    }

    @Override
    public Order selectOne(Order order) {
        if( null == order ) {
            log.warn("select order one, but order is null ...");
            return null;
        }
        order = orderMapper.selectOne( order );
        return order;
    }


    @Override
    public PagingData<Order> selectPage(Order order) {
        PagingData<Order> pagingData = new PagingData<>();

        if( null == order ) {
            log.warn("select order page, but order is null...");
            return pagingData;
        }

        Integer queryCount = orderMapper.selectByIndexCount( order );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select order page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<Order> orders =  selectByIndex( order );
        pagingData.setData( orders );
        return pagingData;
    }

    @Override
    public List<Order> selectByIndex(Order order) {
        List<Order> orders = new ArrayList<>();
        if( null == order ) {
            log.warn("select order by index, but order is null ...");
            return orders;
        }

        orders = orderMapper.selectByIndex( order );

        return orders;
    }
}
