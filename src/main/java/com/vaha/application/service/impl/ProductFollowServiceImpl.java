package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.ProductFollowMapper;
import com.vaha.domain.entity.ProductFollow;
import com.vaha.application.service.ProductFollowService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ProductFollowServiceImpl implements ProductFollowService {

    private final ProductFollowMapper productFollowMapper;

    @Override
    @Transactional
    public Integer save(ProductFollow productFollow) {

        if( null == productFollow ) {
            log.warn("save productFollow, but productFollow is null...");
            return 0;
        }

        return productFollowMapper.save( productFollow );
    }

    @Override
    @Transactional
    public Integer deleteById(ProductFollow productFollow) {

        if( null == productFollow ) {
            log.warn("delete productFollow, but productFollow is null  or productFollow id is null...");
            return 0;
        }

        return productFollowMapper.deleteById( productFollow );
    }

    @Override
    @Transactional
    public Integer updateById(ProductFollow productFollow) {

        if( null == productFollow ) {
            log.warn("update productFollow, but productFollow is null  or productFollow id is null...");
            return 0;
        }

        return productFollowMapper.updateById( productFollow );
    }

    @Override
    public ProductFollow selectOne(ProductFollow productFollow) {
        if( null == productFollow ) {
            log.warn("select productFollow one, but productFollow is null ...");
            return null;
        }
        productFollow = productFollowMapper.selectOne( productFollow );
        return productFollow;
    }


    @Override
    public PagingData<ProductFollow> selectPage(ProductFollow productFollow) {
        PagingData<ProductFollow> pagingData = new PagingData<>();

        if( null == productFollow ) {
            log.warn("select productFollow page, but productFollow is null...");
            return pagingData;
        }

        Integer queryCount = productFollowMapper.selectByIndexCount( productFollow );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select productFollow page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<ProductFollow> productFollows =  selectByIndex( productFollow );
        pagingData.setData( productFollows );
        return pagingData;
    }

    @Override
    public List<ProductFollow> selectByIndex(ProductFollow productFollow) {
        List<ProductFollow> productFollows = new ArrayList<>();
        if( null == productFollow ) {
            log.warn("select productFollow by index, but productFollow is null ...");
            return productFollows;
        }

        productFollows = productFollowMapper.selectByIndex( productFollow );

        return productFollows;
    }

    @Override
    public boolean checkProductFollowUnique(ProductFollow productfollow) {
        productfollow.setUpdator(null);
        productfollow.setUpdateTime(null);
        productfollow.setCreator(null);
        productfollow.setCreateTime(null);
        productfollow.setId(null);
        ProductFollow info = productFollowMapper.checkProductFollowUnique(productfollow);
        if (!Objects.isNull(info)) {
            return false;
        }

        return true;
    }
}
