package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.ProductItemMapper;
import com.vaha.domain.entity.ProductItem;
import com.vaha.application.service.ProductItemService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ProductItemServiceImpl implements ProductItemService {

    private final ProductItemMapper productItemMapper;

    @Override
    @Transactional
    public Integer save(ProductItem productItem) {

        if( null == productItem ) {
            log.warn("save productItem, but productItem is null...");
            return 0;
        }

        return productItemMapper.save( productItem );
    }

    @Override
    @Transactional
    public Integer deleteById(ProductItem productItem) {

        if( null == productItem ) {
            log.warn("delete productItem, but productItem is null  or productItem id is null...");
            return 0;
        }

        return productItemMapper.deleteById( productItem );
    }

    @Override
    @Transactional
    public Integer updateById(ProductItem productItem) {

        if( null == productItem ) {
            log.warn("update productItem, but productItem is null  or productItem id is null...");
            return 0;
        }

        return productItemMapper.updateById( productItem );
    }

    @Override
    public ProductItem selectOne(ProductItem productItem) {
        if( null == productItem ) {
            log.warn("select productItem one, but productItem is null ...");
            return null;
        }
        productItem = productItemMapper.selectOne( productItem );
        return productItem;
    }


    @Override
    public PagingData<ProductItem> selectPage(ProductItem productItem) {
        PagingData<ProductItem> pagingData = new PagingData<>();

        if( null == productItem ) {
            log.warn("select productItem page, but productItem is null...");
            return pagingData;
        }

        Integer queryCount = productItemMapper.selectByIndexCount( productItem );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select productItem page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<ProductItem> productItems =  selectByIndex( productItem );
        pagingData.setData( productItems );
        return pagingData;
    }

    @Override
    public List<ProductItem> selectByIndex(ProductItem productItem) {
        List<ProductItem> productItems = new ArrayList<>();
        if( null == productItem ) {
            log.warn("select productItem by index, but productItem is null ...");
            return productItems;
        }

        productItems = productItemMapper.selectByIndex( productItem );

        return productItems;
    }

    @Override
    public List<ProductItem> selectByCodes(List<String> productItemCodes) {

        return productItemMapper.selectByCodes(productItemCodes);
    }
}
