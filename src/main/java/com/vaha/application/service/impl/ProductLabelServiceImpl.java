package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.ProductLabelMapper;
import com.vaha.domain.entity.ProductLabel;
import com.vaha.application.service.ProductLabelService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ProductLabelServiceImpl implements ProductLabelService {

    private final ProductLabelMapper productLabelMapper;

    @Override
    @Transactional
    public Integer save(ProductLabel productLabel) {

        if( null == productLabel ) {
            log.warn("save productLabel, but productLabel is null...");
            return 0;
        }

        return productLabelMapper.save( productLabel );
    }

    @Override
    @Transactional
    public Integer deleteById(ProductLabel productLabel) {

        if( null == productLabel ) {
            log.warn("delete productLabel, but productLabel is null  or productLabel id is null...");
            return 0;
        }

        return productLabelMapper.deleteById( productLabel );
    }

    @Override
    @Transactional
    public Integer updateById(ProductLabel productLabel) {

        if( null == productLabel ) {
            log.warn("update productLabel, but productLabel is null  or productLabel id is null...");
            return 0;
        }

        return productLabelMapper.updateById( productLabel );
    }

    @Override
    public ProductLabel selectOne(ProductLabel productLabel) {
        if( null == productLabel ) {
            log.warn("select productLabel one, but productLabel is null ...");
            return null;
        }
        productLabel = productLabelMapper.selectOne( productLabel );
        return productLabel;
    }


    @Override
    public PagingData<ProductLabel> selectPage(ProductLabel productLabel) {
        PagingData<ProductLabel> pagingData = new PagingData<>();

        if( null == productLabel ) {
            log.warn("select productLabel page, but productLabel is null...");
            return pagingData;
        }

        Integer queryCount = productLabelMapper.selectByIndexCount( productLabel );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select productLabel page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<ProductLabel> productLabels =  selectByIndex( productLabel );
        pagingData.setData( productLabels );
        return pagingData;
    }

    @Override
    public List<ProductLabel> selectByIndex(ProductLabel productLabel) {
        List<ProductLabel> productLabels = new ArrayList<>();
        if( null == productLabel ) {
            log.warn("select productLabel by index, but productLabel is null ...");
            return productLabels;
        }

        productLabels = productLabelMapper.selectByIndex( productLabel );

        return productLabels;
    }
}
