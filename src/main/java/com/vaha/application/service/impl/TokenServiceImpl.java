package com.vaha.application.service.impl;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.cyber.domain.entity.AuthingToken;
import com.cyber.infrastructure.toolkit.JwtUtils;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.vaha.application.service.TokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * token验证处理
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService<JSONObject> {

    private final static long expireTime = 720;

    /**
     * 创建令牌
     */
    @Override
    public Map<String, Object> createToken(AuthingToken<JSONObject> loginUser) {
        String token = IdUtil.fastUUID();
        String userId = loginUser.getUser().getString("id");
        String userName = loginUser.getUser().getString("name");

        // Jwt存储信息
        Map<String, Object> claimsMap = new HashMap<String, Object>();
        claimsMap.put("token", token);
        claimsMap.put("session_id", userId);
        claimsMap.put("session_name", userName);
        claimsMap.put("tenant_code", "vaha");
        claimsMap.put("user",JSONObject.parseObject(JSONObject.toJSONString(loginUser.getUser())));

        // 接口返回信息
        Map<String, Object> rspMap = new HashMap<String, Object>();
        rspMap.put("access_token", JwtUtils.createToken(claimsMap));
        rspMap.put("expires_in", expireTime);
        return rspMap;
    }

}
