package com.vaha.application.service.impl;

import com.cyber.domain.entity.PagingData;
import com.vaha.application.service.TopupPackageService;
import com.vaha.domain.entity.TopupPackage;
import com.vaha.domain.repository.TopupPackageMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TopupPackageServiceImpl implements TopupPackageService {

    private final TopupPackageMapper topupPackageMapper;

    @Override
    @Transactional
    public Integer save(TopupPackage topupPackage) {

        if( null == topupPackage ) {
            log.warn("save topupPackage, but topupPackage is null...");
            return 0;
        }

        return topupPackageMapper.save( topupPackage );
    }

    @Override
    @Transactional
    public Integer deleteById(TopupPackage topupPackage) {

        if( null == topupPackage ) {
            log.warn("delete topupPackage, but topupPackage is null  or topupPackage id is null...");
            return 0;
        }

        return topupPackageMapper.deleteById( topupPackage );
    }

    @Override
    @Transactional
    public Integer updateById(TopupPackage topupPackage) {

        if( null == topupPackage ) {
            log.warn("update topupPackage, but topupPackage is null  or topupPackage id is null...");
            return 0;
        }

        return topupPackageMapper.updateById( topupPackage );
    }

    @Override
    public TopupPackage selectOne(TopupPackage topupPackage) {
        if( null == topupPackage ) {
            log.warn("select topupPackage one, but topupPackage is null ...");
            return null;
        }
        topupPackage = topupPackageMapper.selectOne( topupPackage );
        return topupPackage;
    }


    @Override
    public PagingData<TopupPackage> selectPage(TopupPackage topupPackage) {
        PagingData<TopupPackage> pagingData = new PagingData<>();

        if( null == topupPackage ) {
            log.warn("select topupPackage page, but topupPackage is null...");
            return pagingData;
        }

        Integer queryCount = topupPackageMapper.selectByIndexCount( topupPackage );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select topupPackage page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<TopupPackage> topupPackages =  selectByIndex( topupPackage );
        pagingData.setData( topupPackages );
        return pagingData;
    }

    @Override
    public List<TopupPackage> selectByIndex(TopupPackage topupPackage) {
        List<TopupPackage> topupPackages = new ArrayList<>();
        if( null == topupPackage ) {
            log.warn("select topupPackage by index, but topupPackage is null ...");
            return topupPackages;
        }

        topupPackages = topupPackageMapper.selectByIndex( topupPackage );

        return topupPackages;
    }
}
