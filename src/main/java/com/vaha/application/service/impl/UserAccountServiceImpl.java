package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserAccountMapper;
import com.vaha.domain.entity.UserAccount;
import com.vaha.application.service.UserAccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserAccountServiceImpl implements UserAccountService {

    private final UserAccountMapper userAccountMapper;

    @Override
    @Transactional
    public Integer save(UserAccount userAccount) {

        if( null == userAccount ) {
            log.warn("save userAccount, but userAccount is null...");
            return 0;
        }

        return userAccountMapper.save( userAccount );
    }

    @Override
    @Transactional
    public Integer deleteById(UserAccount userAccount) {

        if( null == userAccount ) {
            log.warn("delete userAccount, but userAccount is null  or userAccount id is null...");
            return 0;
        }

        return userAccountMapper.deleteById( userAccount );
    }

    @Override
    @Transactional
    public Integer updateById(UserAccount userAccount) {

        if( null == userAccount ) {
            log.warn("update userAccount, but userAccount is null  or userAccount id is null...");
            return 0;
        }

        return userAccountMapper.updateById( userAccount );
    }

    @Override
    public UserAccount selectOne(UserAccount userAccount) {
        if( null == userAccount ) {
            log.warn("select userAccount one, but userAccount is null ...");
            return null;
        }
        userAccount = userAccountMapper.selectOne( userAccount );
        return userAccount;
    }


    @Override
    public PagingData<UserAccount> selectPage(UserAccount userAccount) {
        PagingData<UserAccount> pagingData = new PagingData<>();

        if( null == userAccount ) {
            log.warn("select userAccount page, but userAccount is null...");
            return pagingData;
        }

        Integer queryCount = userAccountMapper.selectByIndexCount( userAccount );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select userAccount page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<UserAccount> userAccounts =  selectByIndex( userAccount );
        pagingData.setData( userAccounts );
        return pagingData;
    }

    @Override
    public List<UserAccount> selectByIndex(UserAccount userAccount) {
        List<UserAccount> userAccounts = new ArrayList<>();
        if( null == userAccount ) {
            log.warn("select userAccount by index, but userAccount is null ...");
            return userAccounts;
        }

        userAccounts = userAccountMapper.selectByIndex( userAccount );

        return userAccounts;
    }
}
