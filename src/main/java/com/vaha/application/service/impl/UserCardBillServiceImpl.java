package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserCardBillMapper;
import com.vaha.domain.entity.UserCardBill;
import com.vaha.application.service.UserCardBillService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserCardBillServiceImpl implements UserCardBillService {

    private final UserCardBillMapper userCardBillMapper;

    @Override
    @Transactional
    public Integer save(UserCardBill userCardBill) {

        if( null == userCardBill ) {
            log.warn("save userCardBill, but userCardBill is null...");
            return 0;
        }

        return userCardBillMapper.save( userCardBill );
    }

    @Override
    @Transactional
    public Integer deleteById(UserCardBill userCardBill) {

        if( null == userCardBill ) {
            log.warn("delete userCardBill, but userCardBill is null  or userCardBill id is null...");
            return 0;
        }

        return userCardBillMapper.deleteById( userCardBill );
    }

    @Override
    @Transactional
    public Integer updateById(UserCardBill userCardBill) {

        if( null == userCardBill ) {
            log.warn("update userCardBill, but userCardBill is null  or userCardBill id is null...");
            return 0;
        }

        return userCardBillMapper.updateById( userCardBill );
    }

    @Override
    public UserCardBill selectOne(UserCardBill userCardBill) {
        if( null == userCardBill ) {
            log.warn("select userCardBill one, but userCardBill is null ...");
            return null;
        }
        userCardBill = userCardBillMapper.selectOne( userCardBill );
        return userCardBill;
    }


    @Override
    public PagingData<UserCardBill> selectPage(UserCardBill userCardBill) {
        PagingData<UserCardBill> pagingData = new PagingData<>();

        if( null == userCardBill ) {
            log.warn("select userCardBill page, but userCardBill is null...");
            return pagingData;
        }

        Integer queryCount = userCardBillMapper.selectByIndexCount( userCardBill );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select userCardBill page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<UserCardBill> userCardBills =  selectByIndex( userCardBill );
        pagingData.setData( userCardBills );
        return pagingData;
    }

    @Override
    public List<UserCardBill> selectByIndex(UserCardBill userCardBill) {
        List<UserCardBill> userCardBills = new ArrayList<>();
        if( null == userCardBill ) {
            log.warn("select userCardBill by index, but userCardBill is null ...");
            return userCardBills;
        }

        userCardBills = userCardBillMapper.selectByIndex( userCardBill );

        return userCardBills;
    }
}
