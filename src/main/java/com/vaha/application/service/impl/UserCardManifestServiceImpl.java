package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserCardManifestMapper;
import com.vaha.domain.entity.UserCardManifest;
import com.vaha.application.service.UserCardManifestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserCardManifestServiceImpl implements UserCardManifestService {

    private final UserCardManifestMapper userCardManifestMapper;

    @Override
    @Transactional
    public Integer save(UserCardManifest userCardManifest) {

        if( null == userCardManifest ) {
            log.warn("save userCardManifest, but userCardManifest is null...");
            return 0;
        }

        return userCardManifestMapper.save( userCardManifest );
    }

    @Override
    @Transactional
    public Integer deleteById(UserCardManifest userCardManifest) {

        if( null == userCardManifest ) {
            log.warn("delete userCardManifest, but userCardManifest is null  or userCardManifest id is null...");
            return 0;
        }

        return userCardManifestMapper.deleteById( userCardManifest );
    }

    @Override
    @Transactional
    public Integer updateById(UserCardManifest userCardManifest) {

        if( null == userCardManifest ) {
            log.warn("update userCardManifest, but userCardManifest is null  or userCardManifest id is null...");
            return 0;
        }

        return userCardManifestMapper.updateById( userCardManifest );
    }

    @Override
    public UserCardManifest selectOne(UserCardManifest userCardManifest) {
        if( null == userCardManifest ) {
            log.warn("select userCardManifest one, but userCardManifest is null ...");
            return null;
        }
        userCardManifest = userCardManifestMapper.selectOne( userCardManifest );
        return userCardManifest;
    }


    @Override
    public PagingData<UserCardManifest> selectPage(UserCardManifest userCardManifest) {
        PagingData<UserCardManifest> pagingData = new PagingData<>();

        if( null == userCardManifest ) {
            log.warn("select userCardManifest page, but userCardManifest is null...");
            return pagingData;
        }

        Integer queryCount = userCardManifestMapper.selectByIndexCount( userCardManifest );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select userCardManifest page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<UserCardManifest> userCardManifests =  selectByIndex( userCardManifest );
        pagingData.setData( userCardManifests );
        return pagingData;
    }

    @Override
    public List<UserCardManifest> selectByIndex(UserCardManifest userCardManifest) {
        List<UserCardManifest> userCardManifests = new ArrayList<>();
        if( null == userCardManifest ) {
            log.warn("select userCardManifest by index, but userCardManifest is null ...");
            return userCardManifests;
        }

        userCardManifests = userCardManifestMapper.selectByIndex( userCardManifest );

        return userCardManifests;
    }
}
