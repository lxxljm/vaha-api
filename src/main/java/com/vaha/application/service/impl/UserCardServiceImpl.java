package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserCardMapper;
import com.vaha.domain.entity.UserCard;
import com.vaha.application.service.UserCardService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserCardServiceImpl implements UserCardService {

    private final UserCardMapper userCardMapper;

    @Override
    @Transactional
    public Integer save(UserCard userCard) {

        if( null == userCard ) {
            log.warn("save userCard, but userCard is null...");
            return 0;
        }

        return userCardMapper.save( userCard );
    }

    @Override
    @Transactional
    public Integer deleteById(UserCard userCard) {

        if( null == userCard ) {
            log.warn("delete userCard, but userCard is null  or userCard id is null...");
            return 0;
        }

        return userCardMapper.deleteById( userCard );
    }

    @Override
    @Transactional
    public Integer updateById(UserCard userCard) {

        if( null == userCard ) {
            log.warn("update userCard, but userCard is null  or userCard id is null...");
            return 0;
        }

        return userCardMapper.updateById( userCard );
    }

    @Override
    public UserCard selectOne(UserCard userCard) {
        if( null == userCard ) {
            log.warn("select userCard one, but userCard is null ...");
            return null;
        }
        userCard = userCardMapper.selectOne( userCard );
        return userCard;
    }


    @Override
    public PagingData<UserCard> selectPage(UserCard userCard) {
        PagingData<UserCard> pagingData = new PagingData<>();

        if( null == userCard ) {
            log.warn("select userCard page, but userCard is null...");
            return pagingData;
        }

        Integer queryCount = userCardMapper.selectByIndexCount( userCard );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select userCard page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<UserCard> userCards =  selectByIndex( userCard );
        pagingData.setData( userCards );
        return pagingData;
    }

    @Override
    public List<UserCard> selectByIndex(UserCard userCard) {
        List<UserCard> userCards = new ArrayList<>();
        if( null == userCard ) {
            log.warn("select userCard by index, but userCard is null ...");
            return userCards;
        }

        userCards = userCardMapper.selectByIndex( userCard );

        return userCards;
    }

    @Override
    public List<UserCard> selectByCodes(List<String> userCardCodes) {
        return userCardMapper.selectByCodes(userCardCodes);
    }

    @Override
    @Transactional
    public Integer bulkSave(List<UserCard> userCards) {
        if( null == userCards ) {
            log.warn("bulkSave userCard by userCards, but userCards is null ...");
            return 0;
        }

        return userCardMapper.bulkSave(userCards);
    }

    @Override
    public Integer bulkDelete(List<UserCard> userCards) {
        return null;
    }

    @Override
    public Integer bulkUpdate(List<UserCard> userCards) {
        return null;
    }
}
