package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserLoginLogMapper;
import com.vaha.domain.entity.UserLoginLog;
import com.vaha.application.service.UserLoginLogService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserLoginLogServiceImpl implements UserLoginLogService {

    private final UserLoginLogMapper userLoginLogMapper;

    @Override
    @Transactional
    public Integer save(UserLoginLog userLoginLog) {

        if( null == userLoginLog ) {
            log.warn("save userLoginLog, but userLoginLog is null...");
            return 0;
        }

        return userLoginLogMapper.save( userLoginLog );
    }

    @Override
    @Transactional
    public Integer deleteById(UserLoginLog userLoginLog) {

        if( null == userLoginLog ) {
            log.warn("delete userLoginLog, but userLoginLog is null  or userLoginLog id is null...");
            return 0;
        }

        return userLoginLogMapper.deleteById( userLoginLog );
    }

    @Override
    @Transactional
    public Integer updateById(UserLoginLog userLoginLog) {

        if( null == userLoginLog ) {
            log.warn("update userLoginLog, but userLoginLog is null  or userLoginLog id is null...");
            return 0;
        }

        return userLoginLogMapper.updateById( userLoginLog );
    }

    @Override
    public UserLoginLog selectOne(UserLoginLog userLoginLog) {
        if( null == userLoginLog ) {
            log.warn("select userLoginLog one, but userLoginLog is null ...");
            return null;
        }
        userLoginLog = userLoginLogMapper.selectOne( userLoginLog );
        return userLoginLog;
    }


    @Override
    public PagingData<UserLoginLog> selectPage(UserLoginLog userLoginLog) {
        PagingData<UserLoginLog> pagingData = new PagingData<>();

        if( null == userLoginLog ) {
            log.warn("select userLoginLog page, but userLoginLog is null...");
            return pagingData;
        }

        Integer queryCount = userLoginLogMapper.selectByIndexCount( userLoginLog );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select userLoginLog page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<UserLoginLog> userLoginLogs =  selectByIndex( userLoginLog );
        pagingData.setData( userLoginLogs );
        return pagingData;
    }

    @Override
    public List<UserLoginLog> selectByIndex(UserLoginLog userLoginLog) {
        List<UserLoginLog> userLoginLogs = new ArrayList<>();
        if( null == userLoginLog ) {
            log.warn("select userLoginLog by index, but userLoginLog is null ...");
            return userLoginLogs;
        }

        userLoginLogs = userLoginLogMapper.selectByIndex( userLoginLog );

        return userLoginLogs;
    }
}
