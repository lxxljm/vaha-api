package	com.vaha.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyber.domain.entity.PagingData;
import com.vaha.domain.repository.UserMapper;
import com.vaha.domain.entity.User;
import com.vaha.application.service.UserService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    @Override
    @Transactional
    public Integer save(User user) {

        if( null == user ) {
            log.warn("save user, but user is null...");
            return 0;
        }

        return userMapper.save( user );
    }

    @Override
    @Transactional
    public Integer deleteById(User user) {

        if( null == user ) {
            log.warn("delete user, but user is null  or user id is null...");
            return 0;
        }

        return userMapper.deleteById( user );
    }

    @Override
    @Transactional
    public Integer updateById(User user) {

        if( null == user ) {
            log.warn("update user, but user is null  or user id is null...");
            return 0;
        }

        return userMapper.updateById( user );
    }

    @Override
    public User selectOne(User user) {
        if( null == user ) {
            log.warn("select user one, but user is null ...");
            return null;
        }
        user = userMapper.selectOne( user );
        return user;
    }


    @Override
    public PagingData<User> selectPage(User user) {
        PagingData<User> pagingData = new PagingData<>();

        if( null == user ) {
            log.warn("select user page, but user is null...");
            return pagingData;
        }

        Integer queryCount = userMapper.selectByIndexCount( user );
        pagingData.setRow( queryCount );

        if( queryCount <= 0 ) {
            log.info("select user page , but count {} == 0 ...",queryCount);
            return pagingData;
        }

        List<User> users =  selectByIndex( user );
        pagingData.setData( users );
        return pagingData;
    }

    @Override
    public List<User> selectByIndex(User user) {
        List<User> users = new ArrayList<>();
        if( null == user ) {
            log.warn("select user by index, but user is null ...");
            return users;
        }

        users = userMapper.selectByIndex( user );

        return users;
    }
}
