package com.vaha.domain.entity;

import com.cyber.domain.entity.PagingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class Category extends PagingEntity {

	

	/**编码*/
	private String code;

	/**名称*/
	private String name;

	/**描述*/
	private String description;

	/**内容列表*/
	private List<CategoryItem> items;
}