package com.vaha.domain.entity;

import com.cyber.domain.entity.PagingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MsTempalte extends PagingEntity {

	
	/**编码*/
	private String code;
	/**类型*/
	private Integer type;
	/**参数*/
	private String various;
	/**内容*/
	private String content;
	
}