package com.vaha.domain.entity;

import com.cyber.domain.entity.PagingEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductLabel extends PagingEntity {

	

	/**编码*/
	private String code;

	/**名称*/
	private String name;
}