package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.CategoryItem;

import java.util.List;

@Mapper
public interface CategoryItemMapper extends BaseMapper<CategoryItem> {

    List<CategoryItem> selectByCodes(List<String> categoryCodes);
}