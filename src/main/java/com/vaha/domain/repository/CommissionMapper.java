package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.Commission;

@Mapper
public interface CommissionMapper extends BaseMapper<Commission> {

}