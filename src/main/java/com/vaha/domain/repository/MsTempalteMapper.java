package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.MsTempalte;

@Mapper
public interface MsTempalteMapper extends BaseMapper<MsTempalte> {

}