package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.Order;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}