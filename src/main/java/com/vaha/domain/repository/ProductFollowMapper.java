package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.ProductFollow;

@Mapper
public interface ProductFollowMapper extends BaseMapper<ProductFollow> {

    ProductFollow checkProductFollowUnique(ProductFollow productfollow);
}