package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.ProductItem;

import java.util.List;

@Mapper
public interface ProductItemMapper extends BaseMapper<ProductItem> {

    List<ProductItem> selectByCodes(List<String> productItemCodes);

}