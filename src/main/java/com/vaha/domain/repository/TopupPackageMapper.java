package com.vaha.domain.repository;

import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.TopupPackage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TopupPackageMapper extends BaseMapper<TopupPackage> {

}