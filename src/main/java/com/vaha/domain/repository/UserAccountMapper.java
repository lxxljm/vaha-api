package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.UserAccount;

@Mapper
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}