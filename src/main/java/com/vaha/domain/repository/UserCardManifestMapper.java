package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.UserCardManifest;

@Mapper
public interface UserCardManifestMapper extends BaseMapper<UserCardManifest> {

}