package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.UserCard;

import java.util.List;

@Mapper
public interface UserCardMapper extends BaseMapper<UserCard> {

    List<UserCard> selectByCodes(List<String> userCardCodes);

    Integer bulkSave(List<UserCard> userCards);
}