package com.vaha.domain.repository;

import org.apache.ibatis.annotations.Mapper;
import com.cyber.infrastructure.repository.BaseMapper;
import com.vaha.domain.entity.UserLoginLog;

@Mapper
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}