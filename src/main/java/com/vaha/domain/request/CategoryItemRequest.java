package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.CategoryItem;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CategoryItemRequest extends PagingRequest {

	

	private String name;

	private String code;

	private Integer type;

	/**
	 * 专题目录id
	 */
	private String parentId;
	
	public CategoryItem toEvent(String tenantCode) {
		CategoryItem categoryItem = new CategoryItem();
		BeanUtils.copyProperties(this, categoryItem);
        categoryItem.setTenantCode(tenantCode);
		return categoryItem;
	}
}