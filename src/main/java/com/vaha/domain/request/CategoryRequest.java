package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Category;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CategoryRequest extends PagingRequest {

	

	private String code;

	private String name;

	public Category toEvent(String tenantCode) {
		Category category = new Category();
		BeanUtils.copyProperties(this, category);
        category.setTenantCode(tenantCode);
		return category;
	}
}