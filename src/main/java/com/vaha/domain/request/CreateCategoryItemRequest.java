package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.CategoryItem;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateCategoryItemRequest extends OperateEntity {

	/**
	 * 专题目录id
	 */
	@NotBlank
	private String categoryCode;

	@NotBlank
	private String name;

	@NotBlank
	private String code;

	@NotNull
	private Integer orderNum;

	@NotNull
	private Integer type;

	@NotBlank
	private String content;

	@NotBlank
	private String covertUrl;

	@NotBlank
	private String linkUrl;

	public CategoryItem toEvent(String userCode,String tenantCode) {
		if(StringUtils.isEmpty(userCode)) {
			userCode = "janusjia";
		}

		if(StringUtils.isEmpty(tenantCode)) {
			tenantCode = "vaha";
		}

		CategoryItem categoryItem = new CategoryItem();
		BeanUtils.copyProperties(this, categoryItem);

		categoryItem.setId(IdUtil.simpleUUID());
        categoryItem.setTenantCode(tenantCode);
        categoryItem.setCreator(userCode);
		categoryItem.setCreateTime(new Date());

        categoryItem.setUpdator(userCode);
		categoryItem.setUpdateTime(new Date());

		return categoryItem;
	}
}
