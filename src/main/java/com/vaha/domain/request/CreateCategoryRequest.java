package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Category;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateCategoryRequest extends OperateEntity {


	@NotBlank
	private String name;

	@NotBlank
	private String code;

	/**描述*/
	private String description;

	public Category toEvent(String userCode,String tenantCode) {
		if(StringUtils.isEmpty(userCode)) {
			userCode = "janusjia";
		}

		if(StringUtils.isEmpty(tenantCode)) {
			tenantCode = "vaha";
		}

		Category category = new Category();
		BeanUtils.copyProperties(this, category);

		category.setId(IdUtil.simpleUUID());
        category.setTenantCode(tenantCode);
        category.setCreator(userCode);
		category.setCreateTime(new Date());

        category.setUpdator(userCode);
		category.setUpdateTime(new Date());

		return category;
	}
}
