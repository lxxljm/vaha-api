package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;
import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.MsTempalte;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateMsTempalteRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**类型*/
	private Integer type;
	/**参数*/
	private String various;
	/**内容*/
	private String content;
	

	public MsTempalte toEvent(String userCode,String tenantCode) {
		MsTempalte msTempalte = new MsTempalte();
		BeanUtils.copyProperties(this, msTempalte);

		msTempalte.setId(IdUtil.simpleUUID());
        msTempalte.setTenantCode(tenantCode);
        msTempalte.setCreator(userCode);
		msTempalte.setCreateTime(new Date());

        msTempalte.setUpdator(userCode);
		msTempalte.setUpdateTime(new Date());

		return msTempalte;
	}
}
