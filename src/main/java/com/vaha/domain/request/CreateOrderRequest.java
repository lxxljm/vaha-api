package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Order;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateOrderRequest extends OperateEntity {
    /**
     * 对象类型
     */
    private Integer productType;
    /**
     * 对象编码
     */
    private String productCode;
    /**
     * 对象内容编码
     */
    private String productItemCode;
    /**
     * 购买类型（手工购买，自动购买）
     */
    private Integer payType;
    /**
     * 购买对象内容
     */
    private JSONObject productDetail;


    public Order toEvent(String userCode, String tenantCode) {
        Order order = new Order();
        BeanUtils.copyProperties(this, order);

        order.setId(IdUtil.simpleUUID());
        order.setTenantCode(tenantCode);
        order.setCreator(userCode);
        order.setCreateTime(new Date());

        order.setUpdator(userCode);
        order.setUpdateTime(new Date());

        return order;
    }
}
