package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductFollow;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateProductFollowRequest extends OperateEntity {


    /**
     * 编码
     */
    private String code;
    @NotNull
    private Integer type;
    /**
     * 对象类型
     */
    @NotNull
    private Integer productType;
    /**
     * 对象编码
     */
    @NotBlank
    private String productCode;
    /**
     * 对象内容编码
     */
    private String productItemCode;
    /**
     * 收藏对象内容
     */
    private String productItemContent;


    public ProductFollow toEvent(String userCode, String tenantCode) {
        ProductFollow productFollow = new ProductFollow();
        BeanUtils.copyProperties(this, productFollow);

        productFollow.setId(IdUtil.simpleUUID());
        productFollow.setTenantCode(tenantCode);
        productFollow.setCreator(userCode);
        productFollow.setCreateTime(new Date());

        productFollow.setUpdator(userCode);
        productFollow.setUpdateTime(new Date());

        return productFollow;
    }
}
