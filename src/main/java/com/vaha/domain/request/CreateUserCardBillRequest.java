package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserCardBill;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateUserCardBillRequest extends OperateEntity {


    /**
     * 编码
     */
    private String cardCode;
    private String userCode;
    /**
     * 币种
     */
    private Integer currency;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 是否冻结
     */
    private Integer freeze;
    /**
     * 解冻时间
     */
    private String unfreezeDate;
    /**
     * 是否处理
     */
    private Integer isDo;
    /**
     * 处理结果
     */
    private String doResult;


    public UserCardBill toEvent(String userCode, String tenantCode) {
        UserCardBill userCardBill = new UserCardBill();
        BeanUtils.copyProperties(this, userCardBill);

        userCardBill.setId(IdUtil.simpleUUID());
        userCardBill.setTenantCode(tenantCode);
        userCardBill.setCreator(userCode);
        userCardBill.setCreateTime(new Date());

        userCardBill.setUpdator(userCode);
        userCardBill.setUpdateTime(new Date());

        return userCardBill;
    }
}
