package com.vaha.domain.request;

import cn.hutool.core.util.IdUtil;
import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.User;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CreateUserRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**名称*/
	private String name;
	/**真名*/
	private String realName;
	/**头像*/
	private String avatar;
	/**性别*/
	private Integer gender;
	/**等级*/
	private String level;
	/**邀请来源*/
	private Integer inviteType;
	/**积分*/
	private String score;
	/**会员*/
	private Integer vip;
	/**会员有效时间*/
	private String vipExipire;
	/**邮箱*/
	private String email;
	/**电话*/
	private String mobile;
	/**生日*/
	private String birthday;
	/**国家*/
	private String country;
	/**地区*/
	private String region;
	/**介绍*/
	private String introduction;
	

	public User toEvent(String userCode,String tenantCode) {
		User user = new User();
		BeanUtils.copyProperties(this, user);

		user.setId(IdUtil.simpleUUID());
        user.setTenantCode(tenantCode);
        user.setCreator(userCode);
		user.setCreateTime(new Date());

        user.setUpdator(userCode);
		user.setUpdateTime(new Date());

		return user;
	}
}
