package com.vaha.domain.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 */
@Getter
@Setter
public class LoginRequest {
    /**
     * 用户账号
     */
    @NotBlank
    private String userName;

    /**
     * 用户密码
     */
    @NotBlank
    private String password;
}
