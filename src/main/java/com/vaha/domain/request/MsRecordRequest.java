package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.MsRecord;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class MsRecordRequest extends PagingRequest {
	
	
	/**发送方式*/
	private Integer type;
	/**发送对象*/
	private String sendTo;
	/**消息编码*/
	private String number;
	/**消息模板*/
	private String templateCode;
	/**消息内容*/
	private String content;
	/**验证次数*/
	private String times;
	/**过期时间*/
	private String expireTime;
	
	
	public MsRecord toEvent(String tenantCode) {
		MsRecord msRecord = new MsRecord();
		BeanUtils.copyProperties(this, msRecord);
        msRecord.setTenantCode(tenantCode);
		return msRecord;
	}
}