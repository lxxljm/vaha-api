package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Order;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class OrderRequest extends PagingRequest {


    /**
     * 编码
     */
    private String code;
    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 对象类型
     */
    private Integer productType;
    /**
     * 对象编码
     */
    private String productCode;
    /**
     * 对象内容编码
     */
    private String productItemCode;
    /**
     * 购买类型（手工购买，自动购买）
     */
    private Integer payType;
    /**
     * 实际价格
     */
    private String transctionAmount;
    /**
     * 有效时间
     */
    private String expireDate;


    public Order toEvent(String tenantCode) {
        Order order = new Order();
        BeanUtils.copyProperties(this, order);
        order.setTenantCode(tenantCode);
        return order;
    }
}