package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductFollow;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ProductFollowRequest extends PagingRequest {

    /**
     * 类型
     */
    private Integer type;

    /**
     * 对象类型
     */
    private Integer productType;
    /**
     * 对象编码
     */
    private String productCode;


    public ProductFollow toEvent(String tenantCode) {
        ProductFollow productFollow = new ProductFollow();
        BeanUtils.copyProperties(this, productFollow);
        productFollow.setTenantCode(tenantCode);
        return productFollow;
    }
}