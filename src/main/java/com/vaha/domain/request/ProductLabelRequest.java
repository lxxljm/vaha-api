package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductLabel;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ProductLabelRequest extends PagingRequest {
	
	
	/**名称*/
	private String name;
	/**编码*/
	private String code;
	
	
	public ProductLabel toEvent(String tenantCode) {
		ProductLabel productLabel = new ProductLabel();
		BeanUtils.copyProperties(this, productLabel);
        productLabel.setTenantCode(tenantCode);
		return productLabel;
	}
}