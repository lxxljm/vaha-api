package com.vaha.domain.request;

import com.cyber.domain.entity.PagingRequest;
import com.vaha.domain.entity.TopupPackage;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class TopupRequest extends PagingRequest {
    /**
     * 编码
     */
    private String topUpPackageCode;

}