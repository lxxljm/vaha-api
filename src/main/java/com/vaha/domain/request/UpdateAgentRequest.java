package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Agent;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateAgentRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**用户编码*/
	private String userCode;
	/**发送方式*/
	private Integer type;
	/**父级代理编码*/
	private String parentCode;
	/**邀请来源*/
	private Integer inviteType;
	/**等级*/
	private String level;
	/**国家*/
	private String country;
	/**地区*/
	private String region;
	/**身份证号码*/
	private String idNumber;
	/**身份证图片*/
	private String idImage;
	

	public Agent toEvent(String userCode,String tenantCode) {
		Agent agent = new Agent();
		BeanUtils.copyProperties(this, agent);

        agent.setTenantCode(tenantCode);
        agent.setUpdator(userCode);
        agent.setUpdateTime(new Date());
		return agent;
	}
}
