package com.vaha.domain.request;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.CategoryItem;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateCategoryItemRequest extends OperateEntity {


    /**
     * 名称
     */
    private String name;

    /**
     * 专题目录id
     */
    private String categoryCode;
    /**
     * 编码
     */
    private String code;
    /**
     * 显示顺序
     */
    private Integer orderNum;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 内容
     */
    private String content;
    /**
     * 海报地址
     */
    private String covertUrl;
    /**
     * URL地址
     */
    private String linkUrl;

    public CategoryItem toEvent(String userCode, String tenantCode) {
        CategoryItem categoryItem = new CategoryItem();
        BeanUtils.copyProperties(this, categoryItem);

        categoryItem.setTenantCode(tenantCode);
        categoryItem.setUpdator(userCode);
        categoryItem.setUpdateTime(new Date());
        return categoryItem;
    }
}
