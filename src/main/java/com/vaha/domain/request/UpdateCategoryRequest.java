package com.vaha.domain.request;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Category;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateCategoryRequest extends OperateEntity {

	

	@NotBlank
	private String code;

	private String name;

	private String description;


	public Category toEvent(String userCode,String tenantCode) {
		if(StringUtils.isEmpty(userCode)) {
			userCode = "janusjia";
		}

		if(StringUtils.isEmpty(tenantCode)) {
			tenantCode = "vaha";
		}


		Category category = new Category();
		BeanUtils.copyProperties(this, category);

        category.setTenantCode(tenantCode);
        category.setUpdator(userCode);
        category.setUpdateTime(new Date());
		return category;
	}
}
