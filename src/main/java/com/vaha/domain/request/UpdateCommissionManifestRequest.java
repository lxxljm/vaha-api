package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.CommissionManifest;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateCommissionManifestRequest extends OperateEntity {

	
	/**事务*/
	private String transction;
	/**代理编码*/
	private String agentCode;
	/**用户事务*/
	private String userTransction;
	/**用户编码*/
	private String userCode;
	/**类型 （充值，消费，任务）*/
	private Integer type;
	/**内容编码*/
	private String contentCode;
	/**当前结算层级*/
	private Integer commissionDeep;
	/**当前结算比例*/
	private String commissionRate;
	/**当前结算金额*/
	private String commissionAmount;
	

	public CommissionManifest toEvent(String userCode,String tenantCode) {
		CommissionManifest commissionManifest = new CommissionManifest();
		BeanUtils.copyProperties(this, commissionManifest);

        commissionManifest.setTenantCode(tenantCode);
        commissionManifest.setUpdator(userCode);
        commissionManifest.setUpdateTime(new Date());
		return commissionManifest;
	}
}
