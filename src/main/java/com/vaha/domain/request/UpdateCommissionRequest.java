package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Commission;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateCommissionRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**产品编码*/
	private String productCode;
	/**代理编码*/
	private String agentCode;
	/**分佣层级*/
	private Integer commissionDeep;
	/**分佣配置*/
	private String commissionConfig;
	/**分佣介绍*/
	private String commissionIntroduction;
	

	public Commission toEvent(String userCode,String tenantCode) {
		Commission commission = new Commission();
		BeanUtils.copyProperties(this, commission);

        commission.setTenantCode(tenantCode);
        commission.setUpdator(userCode);
        commission.setUpdateTime(new Date());
		return commission;
	}
}
