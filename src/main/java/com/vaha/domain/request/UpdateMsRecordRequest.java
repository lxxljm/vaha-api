package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.MsRecord;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateMsRecordRequest extends OperateEntity {

	
	/**发送方式*/
	private Integer type;
	/**发送对象*/
	private String sendTo;
	/**消息编码*/
	private String number;
	/**消息模板*/
	private String templateCode;
	/**消息内容*/
	private String content;
	/**验证次数*/
	private String times;
	/**过期时间*/
	private String expireTime;
	

	public MsRecord toEvent(String userCode,String tenantCode) {
		MsRecord msRecord = new MsRecord();
		BeanUtils.copyProperties(this, msRecord);

        msRecord.setTenantCode(tenantCode);
        msRecord.setUpdator(userCode);
        msRecord.setUpdateTime(new Date());
		return msRecord;
	}
}
