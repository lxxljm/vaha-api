package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.MsTempalte;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateMsTempalteRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**类型*/
	private Integer type;
	/**参数*/
	private String various;
	/**内容*/
	private String content;
	

	public MsTempalte toEvent(String userCode,String tenantCode) {
		MsTempalte msTempalte = new MsTempalte();
		BeanUtils.copyProperties(this, msTempalte);

        msTempalte.setTenantCode(tenantCode);
        msTempalte.setUpdator(userCode);
        msTempalte.setUpdateTime(new Date());
		return msTempalte;
	}
}
