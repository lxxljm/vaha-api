package com.vaha.domain.request;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Order;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateOrderRequest extends OperateEntity {


    /**
     * 编码
     */
    private String code;
    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 对象类型
     */
    private Integer productType;
    /**
     * 对象编码
     */
    private String productCode;
    /**
     * 对象内容编码
     */
    private String productItemCode;
    /**
     * 购买类型（手工购买，自动购买）
     */
    private Integer payType;

    /**
     * 交易币种
     */
    private Integer transctionCurrency;

    /**
     * 交易卡包编码
     */
    private String transctionCardCode;

    /**
     * 交易价格
     */
    private BigDecimal transctionAmount;
    /**
     * 有效时间
     */
    private String expireDate;
    /**
     * 购买对象内容
     */
    private JSONObject productDetail;


    public Order toEvent(String userCode, String tenantCode) {
        Order order = new Order();
        BeanUtils.copyProperties(this, order);

        order.setTenantCode(tenantCode);
        order.setUpdator(userCode);
        order.setUpdateTime(new Date());
        return order;
    }
}
