package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductFollow;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateProductFollowRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	private Integer type;
	/**用户编码*/
	private String userCode;
	/**对象类型*/
	private Integer productType;
	/**对象编码*/
	private String productCode;
	/**对象内容编码*/
	private String productItemCode;
	/**收藏对象内容*/
	private String productItemContent;
	

	public ProductFollow toEvent(String userCode,String tenantCode) {
		ProductFollow productFollow = new ProductFollow();
		BeanUtils.copyProperties(this, productFollow);

        productFollow.setTenantCode(tenantCode);
        productFollow.setUpdator(userCode);
        productFollow.setUpdateTime(new Date());
		return productFollow;
	}
}
