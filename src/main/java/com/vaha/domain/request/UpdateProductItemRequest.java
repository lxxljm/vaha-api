package com.vaha.domain.request;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductItem;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateProductItemRequest extends OperateEntity {


    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String productCode;

    /**
     * 显示顺序
     */
    private Integer orderNum;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 币种
     */
    private Integer currency;
    /**
     * 免费
     */
    private Integer free;
    /**
     * 价格
     */
    private String amount;
    /**
     * 描述
     */
    private String description;
    /**
     * 规格
     */
    private String specs;
    /**
     * 统计信息
     */
    private String stat;
    /**
     * 详细内容
     */
    private String detail;


    public ProductItem toEvent(String userCode, String tenantCode) {
        ProductItem productItem = new ProductItem();
        BeanUtils.copyProperties(this, productItem);

        productItem.setTenantCode(tenantCode);
        productItem.setUpdator(userCode);
        productItem.setUpdateTime(new Date());
        return productItem;
    }
}
