package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.ProductLabel;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateProductLabelRequest extends OperateEntity {

	
	/**名称*/
	private String name;
	/**编码*/
	private String code;
	

	public ProductLabel toEvent(String userCode,String tenantCode) {
		ProductLabel productLabel = new ProductLabel();
		BeanUtils.copyProperties(this, productLabel);

        productLabel.setTenantCode(tenantCode);
        productLabel.setUpdator(userCode);
        productLabel.setUpdateTime(new Date());
		return productLabel;
	}
}
