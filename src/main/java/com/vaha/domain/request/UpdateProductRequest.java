package com.vaha.domain.request;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.Product;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateProductRequest extends OperateEntity {

	
	/**名称*/
	private String name;
	/**编码*/
	private String code;
	/**类型*/
	private Integer type;
	/**海报*/
	private String covert;
	/**简介*/
	private String summary;
	/**描述*/
	private String description;
	/**价格*/
	private BigDecimal price;
	/**总数*/
	private String amount;
	/**规格*/
	private String specs;
	/**标签*/
	private String label;
	/**统计信息*/
	private String stat;
	

	public Product toEvent(String userCode,String tenantCode) {
		Product product = new Product();
		BeanUtils.copyProperties(this, product);

        product.setTenantCode(tenantCode);
        product.setUpdator(userCode);
        product.setUpdateTime(new Date());
		return product;
	}
}
