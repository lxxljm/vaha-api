package com.vaha.domain.request;

import com.cyber.domain.entity.OperateEntity;
import com.vaha.domain.entity.TopupPackage;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateTopupPackageRequest extends OperateEntity {


    /**
     * 名称
     */
    private String name;
    /**
     * 编码
     */
    private String code;
    /**
     * From金额
     */
    private BigDecimal fromAmount;
    /**
     * From币种
     */
    private Integer fromCurrency;
    /**
     * To金额
     */
    private BigDecimal toAmount;
    /**
     * To币种
     */
    private Integer toCurrency;
    /**
     * 折扣币种
     */
    private Integer opsCurrency;
    /**
     * 折扣
     */
    private BigDecimal opsAmount;
    /**
     * 项目编码
     */
    private String projectCode;

    public TopupPackage toEvent(String userCode, String tenantCode) {
        TopupPackage topupPackage = new TopupPackage();
        BeanUtils.copyProperties(this, topupPackage);

        topupPackage.setTenantCode(tenantCode);
        topupPackage.setUpdator(userCode);
        topupPackage.setUpdateTime(new Date());
        return topupPackage;
    }
}
