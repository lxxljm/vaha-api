package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserAccount;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateUserAccountRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**用户编码*/
	private String userCode;
	/**类型*/
	private Integer type;
	/**账号*/
	private String account;
	/**账号组*/
	private String groupAccount;
	/**密码盐*/
	private String salt;
	/**密码*/
	private String password;
	/**锁定*/
	private Integer lockd;
	/**锁定时间*/
	private String lockExpire;
	/**失败次数*/
	private String failTimes;
	

	public UserAccount toEvent(String userCode,String tenantCode) {
		UserAccount userAccount = new UserAccount();
		BeanUtils.copyProperties(this, userAccount);

        userAccount.setTenantCode(tenantCode);
        userAccount.setUpdator(userCode);
        userAccount.setUpdateTime(new Date());
		return userAccount;
	}
}
