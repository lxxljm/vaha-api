package com.vaha.domain.request;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserCardManifest;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateUserCardManifestRequest extends OperateEntity {


    /**
     * 事务
     */
    private String transction;
    /**
     * 用户编码
     */
    private String userCode;
    private String contentCode;
    /**
     * From币种
     */
    private Integer fromCurrency;
    private Integer type;
    /**
     * To币种
     */
    private Integer toCurrency;
    /**
     * 渠道
     */
    private String channel;
    /**
     * 兑换公式
     */
    private String currencyRate;
    /**
     * 交易名称
     */
    private String transctionName;
    /**
     * 交易摘要
     */
    private String transctionSummary;
    /**
     * From金额
     */
    private BigDecimal fromAmount;
    /**
     * To金额
     */
    private BigDecimal toAmount;
    private String toCardCode;
    private Integer opsCurrency;
    private String opsCardCode;
    private BigDecimal opsAmount;
    /**
     * 套餐
     */
    private Integer packageInfo;
    /**
     * 套餐详情
     */
    private String packageContent;


    public UserCardManifest toEvent(String userCode, String tenantCode) {
        UserCardManifest userCardManifest = new UserCardManifest();
        BeanUtils.copyProperties(this, userCardManifest);

        userCardManifest.setTenantCode(tenantCode);
        userCardManifest.setUpdator(userCode);
        userCardManifest.setUpdateTime(new Date());
        return userCardManifest;
    }
}
