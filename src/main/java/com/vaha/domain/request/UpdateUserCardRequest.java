package com.vaha.domain.request;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserCard;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateUserCardRequest extends OperateEntity {


    /**
     * 编码
     */
    private String code;
    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 名称
     */
    private String name;
    /**
     * 币种
     */
    private Integer currency;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 图标
     */
    private String icon;
    /**
     * 解冻余额
     */
    private String balance;
    /**
     * 冻结
     */
    private Integer freeze;
    /**
     * 登录结果内容
     */
    private String resultContent;


    public UserCard toEvent(String userCode, String tenantCode) {
        UserCard userCard = new UserCard();
        BeanUtils.copyProperties(this, userCard);

        userCard.setTenantCode(tenantCode);
        userCard.setUpdator(userCode);
        userCard.setUpdateTime(new Date());
        return userCard;
    }
}
