package com.vaha.domain.request;

import java.util.Date;
import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.OperateEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserLoginLog;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UpdateUserLoginLogRequest extends OperateEntity {

	
	/**编码*/
	private String code;
	/**用户编码*/
	private String userCode;
	/**类型*/
	private Integer type;
	/**账号*/
	private String account;
	/**账号组*/
	private String groupAccount;
	/**登录时间*/
	private String loginTime;
	/**登录ip*/
	private String loginIp;
	/**登录信息*/
	private String loginUa;
	/**登录结果*/
	private String result;
	/**登录结果内容*/
	private String resultContent;
	

	public UserLoginLog toEvent(String userCode,String tenantCode) {
		UserLoginLog userLoginLog = new UserLoginLog();
		BeanUtils.copyProperties(this, userLoginLog);

        userLoginLog.setTenantCode(tenantCode);
        userLoginLog.setUpdator(userCode);
        userLoginLog.setUpdateTime(new Date());
		return userLoginLog;
	}
}
