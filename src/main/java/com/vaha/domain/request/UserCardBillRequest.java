package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserCardBill;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UserCardBillRequest extends PagingRequest {


    /**
     * 编码
     */
    @NotBlank
    private String cardCode;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 是否冻结
     */
    private Integer freeze;
    /**
     * 是否处理
     */
    private Integer isDo;


    public UserCardBill toEvent(String tenantCode) {
        UserCardBill userCardBill = new UserCardBill();
        BeanUtils.copyProperties(this, userCardBill);
        userCardBill.setTenantCode(tenantCode);
        return userCardBill;
    }
}