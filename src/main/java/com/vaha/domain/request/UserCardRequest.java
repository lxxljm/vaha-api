package com.vaha.domain.request;

import org.springframework.beans.BeanUtils;
import com.cyber.domain.entity.PagingRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.vaha.domain.entity.UserCard;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UserCardRequest extends PagingRequest {


    /**
     * 编码
     */
    private String code;
    /**
     * 用户编码
     */
    private String userCode;

    /**
     * 名称
     */
    private String name;
    /**
     * 币种
     */
    private Integer currency;
    /**
     * 类型
     */
    private Integer type;


    public UserCard toEvent(String tenantCode) {
        UserCard userCard = new UserCard();
        BeanUtils.copyProperties(this, userCard);
        userCard.setTenantCode(tenantCode);
        return userCard;
    }
}