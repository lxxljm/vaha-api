package com.vaha.infrastructure.config;

import lombok.Data;

@Data
public class ThirdClient {

    private String clientId;

    private String clientSecret;

    private String redirectUri;

    private String proxyHostName;

    private Integer proxyPort;

}
