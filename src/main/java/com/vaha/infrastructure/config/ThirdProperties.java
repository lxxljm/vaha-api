package com.vaha.infrastructure.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@Component
@ConfigurationProperties(prefix = "third")
public class ThirdProperties {

    private ThirdClient google;

    private ThirdClient facebook;

    private ThirdClient twitter;


}
