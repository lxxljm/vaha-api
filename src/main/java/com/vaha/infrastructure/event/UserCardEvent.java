package com.vaha.infrastructure.event;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserCardEvent {

  /** 该类型事件携带的信息 */
  public String userCode;


}