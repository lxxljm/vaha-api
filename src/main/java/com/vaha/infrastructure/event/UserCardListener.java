package com.vaha.infrastructure.event;

import com.vaha.application.service.UserCardService;
import com.vaha.domain.entity.UserCard;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
@AllArgsConstructor
public class UserCardListener {


    private final UserCardService userCardService;


    @Async
    @SneakyThrows
    @EventListener(UserCardEvent.class)
    public void handleMsg(UserCardEvent event) {
        String userCode = event.getUserCode();
        long start = System.currentTimeMillis();

        UserCard userCoinsCard = new UserCard();
        userCoinsCard.toEvent(userCode, 0);

        UserCard userBonusesCard = new UserCard();
        userBonusesCard.toEvent(userCode, 1);

        userCardService.bulkSave(Arrays.asList(userBonusesCard, userCoinsCard));

        long end = System.currentTimeMillis();
        log.info("{}：创建卡包耗时：({})毫秒", userCode, (end - start));
    }
}