package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.CommissionManifestService;
import com.vaha.domain.entity.CommissionManifest;
import com.vaha.domain.request.CommissionManifestRequest;
import com.vaha.domain.request.CreateCommissionManifestRequest;
import com.vaha.domain.request.UpdateCommissionManifestRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class CommissionManifestRest extends AuthingTokenController{

	private final CommissionManifestService commissionManifestService;

	@GetMapping("/commissionmanifest/search")
	public Response searchCommissionManifest(@Valid CommissionManifestRequest request) {
		DataResponse<PagingData<CommissionManifest>> response = new DataResponse<>();
        CommissionManifest  commissionmanifest = request.toEvent(getTenantCode());
		PagingData<CommissionManifest> commissionManifestPage = commissionManifestService.selectPage(commissionmanifest);
		response.setData(commissionManifestPage);
		return response;
	}


	@GetMapping("/commissionmanifest")
	public Response selectOneCommissionManifest(@Valid IdRequest idRequest) {
		DataResponse<CommissionManifest> response = new DataResponse<>();

		CommissionManifest commissionManifest = new CommissionManifest();
		commissionManifest.setId(idRequest.getId());
        commissionManifest.setTenantCode(getTenantCode());
		commissionManifest = commissionManifestService.selectOne(commissionManifest);

		response.setData(commissionManifest);
		return response;
	}

	@PostMapping("/commissionmanifest")
	public Response saveCommissionManifest(@RequestBody @Valid CreateCommissionManifestRequest request) {
	    CommissionManifest  commissionmanifest = request.toEvent(getSessionId(),getTenantCode());

		int result = commissionManifestService.save(commissionmanifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/commissionmanifest")
	public Response updateCommissionManifest(@RequestBody @Valid UpdateCommissionManifestRequest request) {
	    CommissionManifest  commissionmanifest = request.toEvent(getSessionId(),getTenantCode());
		int result = commissionManifestService.updateById(commissionmanifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/commissionmanifest")
	public Response deleteCommissionManifest(@Valid IdRequest idRequest) {
		CommissionManifest commissionManifest = new CommissionManifest();
		commissionManifest.setId(idRequest.getId());

		commissionManifest.setTenantCode(getTenantCode());
		commissionManifest.setUpdator(getSessionId());
        commissionManifest.setUpdateTime(new Date());

		int result = commissionManifestService.deleteById(commissionManifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
