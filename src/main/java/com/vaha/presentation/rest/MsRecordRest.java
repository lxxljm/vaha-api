package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.MsRecordService;
import com.vaha.domain.entity.MsRecord;
import com.vaha.domain.request.CreateMsRecordRequest;
import com.vaha.domain.request.MsRecordRequest;
import com.vaha.domain.request.UpdateMsRecordRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class MsRecordRest extends AuthingTokenController{

	private final MsRecordService msRecordService;

	@GetMapping("/msrecord/search")
	public Response searchMsRecord(@Valid MsRecordRequest request) {
		DataResponse<PagingData<MsRecord>> response = new DataResponse<>();
        MsRecord  msrecord = request.toEvent(getTenantCode());
		PagingData<MsRecord> msRecordPage = msRecordService.selectPage(msrecord);
		response.setData(msRecordPage);
		return response;
	}


	@GetMapping("/msrecord")
	public Response selectOneMsRecord(@Valid IdRequest idRequest) {
		DataResponse<MsRecord> response = new DataResponse<>();

		MsRecord msRecord = new MsRecord();
		msRecord.setId(idRequest.getId());
        msRecord.setTenantCode(getTenantCode());
		msRecord = msRecordService.selectOne(msRecord);

		response.setData(msRecord);
		return response;
	}

	@PostMapping("/msrecord")
	public Response saveMsRecord(@RequestBody @Valid CreateMsRecordRequest request) {
	    MsRecord  msrecord = request.toEvent(getSessionId(),getTenantCode());

		int result = msRecordService.save(msrecord);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/msrecord")
	public Response updateMsRecord(@RequestBody @Valid UpdateMsRecordRequest request) {
	    MsRecord  msrecord = request.toEvent(getSessionId(),getTenantCode());
		int result = msRecordService.updateById(msrecord);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/msrecord")
	public Response deleteMsRecord(@Valid IdRequest idRequest) {
		MsRecord msRecord = new MsRecord();
		msRecord.setId(idRequest.getId());

		msRecord.setTenantCode(getTenantCode());
		msRecord.setUpdator(getSessionId());
        msRecord.setUpdateTime(new Date());

		int result = msRecordService.deleteById(msRecord);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
