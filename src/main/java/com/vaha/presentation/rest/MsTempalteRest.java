package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.MsTempalteService;
import com.vaha.domain.entity.MsTempalte;
import com.vaha.domain.request.CreateMsTempalteRequest;
import com.vaha.domain.request.MsTempalteRequest;
import com.vaha.domain.request.UpdateMsTempalteRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class MsTempalteRest extends AuthingTokenController{

	private final MsTempalteService msTempalteService;

	@GetMapping("/mstempalte/search")
	public Response searchMsTempalte(@Valid MsTempalteRequest request) {
		DataResponse<PagingData<MsTempalte>> response = new DataResponse<>();
        MsTempalte  mstempalte = request.toEvent(getTenantCode());
		PagingData<MsTempalte> msTempaltePage = msTempalteService.selectPage(mstempalte);
		response.setData(msTempaltePage);
		return response;
	}


	@GetMapping("/mstempalte")
	public Response selectOneMsTempalte(@Valid IdRequest idRequest) {
		DataResponse<MsTempalte> response = new DataResponse<>();

		MsTempalte msTempalte = new MsTempalte();
		msTempalte.setId(idRequest.getId());
        msTempalte.setTenantCode(getTenantCode());
		msTempalte = msTempalteService.selectOne(msTempalte);

		response.setData(msTempalte);
		return response;
	}

	@PostMapping("/mstempalte")
	public Response saveMsTempalte(@RequestBody @Valid CreateMsTempalteRequest request) {
	    MsTempalte  mstempalte = request.toEvent(getSessionId(),getTenantCode());

		int result = msTempalteService.save(mstempalte);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/mstempalte")
	public Response updateMsTempalte(@RequestBody @Valid UpdateMsTempalteRequest request) {
	    MsTempalte  mstempalte = request.toEvent(getSessionId(),getTenantCode());
		int result = msTempalteService.updateById(mstempalte);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/mstempalte")
	public Response deleteMsTempalte(@Valid IdRequest idRequest) {
		MsTempalte msTempalte = new MsTempalte();
		msTempalte.setId(idRequest.getId());

		msTempalte.setTenantCode(getTenantCode());
		msTempalte.setUpdator(getSessionId());
        msTempalte.setUpdateTime(new Date());

		int result = msTempalteService.deleteById(msTempalte);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
