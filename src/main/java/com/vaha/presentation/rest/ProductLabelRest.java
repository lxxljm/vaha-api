package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.ProductLabelService;
import com.vaha.domain.entity.ProductLabel;
import com.vaha.domain.request.CreateProductLabelRequest;
import com.vaha.domain.request.ProductLabelRequest;
import com.vaha.domain.request.UpdateProductLabelRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class ProductLabelRest extends AuthingTokenController {

	private final ProductLabelService productLabelService;


	@GetMapping("/user/productlabel/search")
	public Response searchUserProductLabel(@Valid ProductLabelRequest request) {
		DataResponse<PagingData<ProductLabel>> response = new DataResponse<>();
        ProductLabel  productlabel = request.toEvent(getTenantCode());
		PagingData<ProductLabel> productLabelPage = productLabelService.selectPage(productlabel);
		response.setData(productLabelPage);
		return response;
	}


	@GetMapping("/admin/productlabel/search")
	public Response searchProductLabel(@Valid ProductLabelRequest request) {
		DataResponse<PagingData<ProductLabel>> response = new DataResponse<>();
        ProductLabel  productlabel = request.toEvent(getTenantCode());
		PagingData<ProductLabel> productLabelPage = productLabelService.selectPage(productlabel);
		response.setData(productLabelPage);
		return response;
	}


	@GetMapping("/admin/productlabel")
	public Response selectOneProductLabel(@Valid IdRequest idRequest) {
		DataResponse<ProductLabel> response = new DataResponse<>();

		ProductLabel productLabel = new ProductLabel();
		productLabel.setId(idRequest.getId());
        productLabel.setTenantCode(getTenantCode());
		productLabel = productLabelService.selectOne(productLabel);

		response.setData(productLabel);
		return response;
	}

	@PostMapping("/admin/productlabel")
	public Response saveProductLabel(@RequestBody @Valid CreateProductLabelRequest request) {
	    ProductLabel  productlabel = request.toEvent(getSessionId(),getTenantCode());

		int result = productLabelService.save(productlabel);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/admin/productlabel")
	public Response updateProductLabel(@RequestBody @Valid UpdateProductLabelRequest request) {
	    ProductLabel  productlabel = request.toEvent(getSessionId(),getTenantCode());
		int result = productLabelService.updateById(productlabel);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/admin/productlabel")
	public Response deleteProductLabel(@Valid IdRequest idRequest) {
		ProductLabel productLabel = new ProductLabel();
		productLabel.setId(idRequest.getId());

		productLabel.setTenantCode(getTenantCode());
		productLabel.setUpdator(getSessionId());
        productLabel.setUpdateTime(new Date());

		int result = productLabelService.deleteById(productLabel);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
