package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.TopupPackageService;
import com.vaha.domain.entity.TopupPackage;
import com.vaha.domain.request.CreateTopupPackageRequest;
import com.vaha.domain.request.TopupPackageRequest;
import com.vaha.domain.request.UpdateTopupPackageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class TopupPackageRest extends AuthingTokenController{

	private final TopupPackageService topupPackageService;

	@GetMapping("/user/topuppackage/search")
	public Response searchUserTopupPackage(@Valid TopupPackageRequest request) {
		DataResponse<PagingData<TopupPackage>> response = new DataResponse<>();
        TopupPackage  topuppackage = request.toEvent(request.getTenantCode());
		PagingData<TopupPackage> topupPackagePage = topupPackageService.selectPage(topuppackage);
		response.setData(topupPackagePage);
		return response;
	}

	@GetMapping("/admin/topuppackage/search")
	public Response searchTopupPackage(@Valid TopupPackageRequest request) {
		DataResponse<PagingData<TopupPackage>> response = new DataResponse<>();
        TopupPackage  topuppackage = request.toEvent(request.getTenantCode());
		PagingData<TopupPackage> topupPackagePage = topupPackageService.selectPage(topuppackage);
		response.setData(topupPackagePage);
		return response;
	}


	@GetMapping("/admin/topuppackage")
	public Response selectOneTopupPackage(@Valid IdRequest idRequest) {
		DataResponse<TopupPackage> response = new DataResponse<>();

		TopupPackage topupPackage = new TopupPackage();
		topupPackage.setId(idRequest.getId());
        topupPackage.setTenantCode(idRequest.getTenantCode());
		topupPackage = topupPackageService.selectOne(topupPackage);

		response.setData(topupPackage);
		return response;
	}

	@PostMapping("/admin/topuppackage")
	public Response saveTopupPackage(@RequestBody @Valid CreateTopupPackageRequest request) {
	    TopupPackage  topuppackage = request.toEvent(getSessionId(),request.getTenantCode());

		int result = topupPackageService.save(topuppackage);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/admin/topuppackage")
	public Response updateTopupPackage(@RequestBody @Valid UpdateTopupPackageRequest request) {
	    TopupPackage  topuppackage = request.toEvent(getSessionId(),request.getTenantCode());
		int result = topupPackageService.updateById(topuppackage);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/admin/topuppackage")
	public Response deleteTopupPackage(@Valid IdRequest idRequest) {
		TopupPackage topupPackage = new TopupPackage();
		topupPackage.setId(idRequest.getId());

		topupPackage.setTenantCode(idRequest.getTenantCode());
		topupPackage.setUpdator(getSessionId());
        topupPackage.setUpdateTime(new Date());

		int result = topupPackageService.deleteById(topupPackage);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
