package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.UserCardManifestService;
import com.vaha.domain.entity.UserCardManifest;
import com.vaha.domain.request.CreateUserCardManifestRequest;
import com.vaha.domain.request.UpdateUserCardManifestRequest;
import com.vaha.domain.request.UserCardManifestRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class UserCardManifestRest extends AuthingTokenController {

	private final UserCardManifestService userCardManifestService;

	@GetMapping("/usercardmanifest/search")
	public Response searchUserCardManifest(@Valid UserCardManifestRequest request) {
		DataResponse<PagingData<UserCardManifest>> response = new DataResponse<>();
        UserCardManifest  usercardmanifest = request.toEvent(getTenantCode());
		PagingData<UserCardManifest> userCardManifestPage = userCardManifestService.selectPage(usercardmanifest);
		response.setData(userCardManifestPage);
		return response;
	}


	@GetMapping("/usercardmanifest")
	public Response selectOneUserCardManifest(@Valid IdRequest idRequest) {
		DataResponse<UserCardManifest> response = new DataResponse<>();

		UserCardManifest userCardManifest = new UserCardManifest();
		userCardManifest.setId(idRequest.getId());
        userCardManifest.setTenantCode(getTenantCode());
		userCardManifest = userCardManifestService.selectOne(userCardManifest);

		response.setData(userCardManifest);
		return response;
	}

	@PostMapping("/usercardmanifest")
	public Response saveUserCardManifest(@RequestBody @Valid CreateUserCardManifestRequest request) {
	    UserCardManifest  usercardmanifest = request.toEvent(getSessionId(),getTenantCode());

		int result = userCardManifestService.save(usercardmanifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/usercardmanifest")
	public Response updateUserCardManifest(@RequestBody @Valid UpdateUserCardManifestRequest request) {
	    UserCardManifest  usercardmanifest = request.toEvent(getSessionId(),getTenantCode());
		int result = userCardManifestService.updateById(usercardmanifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/usercardmanifest")
	public Response deleteUserCardManifest(@Valid IdRequest idRequest) {
		UserCardManifest userCardManifest = new UserCardManifest();
		userCardManifest.setId(idRequest.getId());

		userCardManifest.setTenantCode(getTenantCode());
		userCardManifest.setUpdator(getSessionId());
        userCardManifest.setUpdateTime(new Date());

		int result = userCardManifestService.deleteById(userCardManifest);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
