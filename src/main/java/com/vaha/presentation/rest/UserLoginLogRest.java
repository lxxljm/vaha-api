package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.UserLoginLogService;
import com.vaha.domain.entity.UserLoginLog;
import com.vaha.domain.request.CreateUserLoginLogRequest;
import com.vaha.domain.request.UpdateUserLoginLogRequest;
import com.vaha.domain.request.UserLoginLogRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequiredArgsConstructor
public class UserLoginLogRest extends AuthingTokenController {

	private final UserLoginLogService userLoginLogService;

	@GetMapping("/userloginlog/search")
	public Response searchUserLoginLog(@Valid UserLoginLogRequest request) {
		DataResponse<PagingData<UserLoginLog>> response = new DataResponse<>();
        UserLoginLog  userloginlog = request.toEvent(getTenantCode());
		PagingData<UserLoginLog> userLoginLogPage = userLoginLogService.selectPage(userloginlog);
		response.setData(userLoginLogPage);
		return response;
	}


	@GetMapping("/userloginlog")
	public Response selectOneUserLoginLog(@Valid IdRequest idRequest) {
		DataResponse<UserLoginLog> response = new DataResponse<>();

		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLog.setId(idRequest.getId());
        userLoginLog.setTenantCode(getTenantCode());
		userLoginLog = userLoginLogService.selectOne(userLoginLog);

		response.setData(userLoginLog);
		return response;
	}

	@PostMapping("/userloginlog")
	public Response saveUserLoginLog(@RequestBody @Valid CreateUserLoginLogRequest request) {
	    UserLoginLog  userloginlog = request.toEvent(getSessionId(),getTenantCode());

		int result = userLoginLogService.save(userloginlog);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@PutMapping("/userloginlog")
	public Response updateUserLoginLog(@RequestBody @Valid UpdateUserLoginLogRequest request) {
	    UserLoginLog  userloginlog = request.toEvent(getSessionId(),getTenantCode());
		int result = userLoginLogService.updateById(userloginlog);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}

	@DeleteMapping("/userloginlog")
	public Response deleteUserLoginLog(@Valid IdRequest idRequest) {
		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLog.setId(idRequest.getId());

		userLoginLog.setTenantCode(getTenantCode());
		userLoginLog.setUpdator(getSessionId());
        userLoginLog.setUpdateTime(new Date());

		int result = userLoginLogService.deleteById(userLoginLog);
		if (result < 1) {
			return Response.fail(HttpResultCode.SERVER_ERROR);
		}
		return Response.success();
	}
}
