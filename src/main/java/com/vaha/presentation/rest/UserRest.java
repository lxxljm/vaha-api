package com.vaha.presentation.rest;

import com.cyber.application.controller.AuthingTokenController;
import com.cyber.domain.constant.HttpResultCode;
import com.cyber.domain.entity.DataResponse;
import com.cyber.domain.entity.IdRequest;
import com.cyber.domain.entity.PagingData;
import com.cyber.domain.entity.Response;
import com.vaha.application.service.UserCardService;
import com.vaha.application.service.UserService;
import com.vaha.domain.entity.User;
import com.vaha.domain.entity.UserCard;
import com.vaha.domain.request.CreateUserRequest;
import com.vaha.domain.request.UpdateUserRequest;
import com.vaha.domain.request.UserRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
public class UserRest extends AuthingTokenController {

    private final UserService userService;

    private final UserCardService userCardService;


    /**
     * 个人信息
     */
    @GetMapping("/user/profile")
    public Response profile() {

        DataResponse<User> response = new DataResponse<>();
        User user = new User();
        user.setId(getUser().getString("id"));

        user = userService.selectOne(user);
        if (!Objects.isNull(user)) {
            UserCard userCard = new UserCard();
            userCard.setLimit(Integer.MAX_VALUE);
            userCard.setUserCode(user.getCode());
            List<UserCard> userCards = userCardService.selectByIndex(userCard);
            user.setUserCards(userCards);
        }
        response.setData(user);
        return response;
    }


    @GetMapping("/user/search")
    public Response searchUser(@Valid UserRequest request) {

        DataResponse<PagingData<User>> response = new DataResponse<>();
        User user = request.toEvent(getTenantCode());
        PagingData<User> userPage = userService.selectPage(user);
        response.setData(userPage);
        return response;
    }


    @GetMapping("/user")
    public Response selectOneUser(@Valid IdRequest idRequest) {
        DataResponse<User> response = new DataResponse<>();

        User user = new User();
        user.setId(idRequest.getId());
        user.setTenantCode(getTenantCode());
        user = userService.selectOne(user);

        response.setData(user);
        return response;
    }

    @PostMapping("/user")
    public Response saveUser(@RequestBody @Valid CreateUserRequest request) {
        User user = request.toEvent(getSessionId(), getTenantCode());

        int result = userService.save(user);
        if (result < 1) {
            return Response.fail(HttpResultCode.SERVER_ERROR);
        }
        return Response.success();
    }

    @PutMapping("/user")
    public Response updateUser(@RequestBody @Valid UpdateUserRequest request) {
        User user = request.toEvent(getSessionId(), getTenantCode());
        int result = userService.updateById(user);
        if (result < 1) {
            return Response.fail(HttpResultCode.SERVER_ERROR);
        }
        return Response.success();
    }

    @DeleteMapping("/user")
    public Response deleteUser(@Valid IdRequest idRequest) {
        User user = new User();
        user.setId(idRequest.getId());

        user.setTenantCode(getTenantCode());
        user.setUpdator(getSessionId());
        user.setUpdateTime(new Date());

        int result = userService.deleteById(user);
        if (result < 1) {
            return Response.fail(HttpResultCode.SERVER_ERROR);
        }
        return Response.success();
    }
}
