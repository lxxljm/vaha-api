
delimiter $$ # 定义结束符
drop procedure if exists addTestData;
# 存储过程名叫：addTestData
create procedure addTestData(in total int, in codeData varchar(100), in itemId varchar(100), in url varchar(512))
begin
    DECLARE number INT;
    set number = 1;
    while number <= total #插入N条数据
        do
            INSERT INTO vaha.t_product_item (id, name, code, order_num, type, currency, free, amount, description,
                                             specs, stat,
                                             detail, tenant_code, project_code, deleted, creator, create_time, updator,
                                             update_time,
                                             remark, product_code)
            VALUES (concat(itemId, '-', number), concat('第', concat(number, '集')), concat(codeData, '-', number),
                    number, 1, 2, 0, '100', '内详', '{
                "cost": 40,
                "size": 6274872
              }', '{
                "buy": 1335,
                "like": 20017,
                "view": 21336,
                "follow": 6283
              }', '{
                "en": {
                  "salt": "232",
                  "cover": "",
                  "480_url": "",
                  "720_url": "",
                  "1080_url": ""
                },
                "ma": {
                  "salt": 232,
                  "cover": "",
                  "480_url": "",
                  "720_url": "",
                  "1080_url": ""
                },
                "zh": {
                  "salt": "232",
                  "cover": "",
                  "480_url": "",
                  "720_url": "",
                  "1080_url": ""
                }
              }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 07:36:21', 'janusjia', '2023-10-14 07:36:21', 'janusjia',
                    codeData);
            set number = number + 1;

end while;
update t_product_item
set detail = json_set(detail, '$."en"."720_url"',
                      concat(url, order_num, '.mp4')),
    detail = json_set(detail, '$."ma"."720_url"',
                      concat(url, order_num, '.mp4')),
    detail = json_set(detail, '$."zh"."720_url"',
                      concat(url, order_num, '.mp4')),
    free   = 1
where product_code = codeData;
end $$;


delimiter $$ # 定义结束符
drop procedure if exists createProductItemData;
CREATE PROCEDURE createProductItemData()
BEGIN
    DECLARE amount int;
    DECLARE code VARCHAR(100);
    DECLARE id VARCHAR(100);
    DECLARE remark VARCHAR(512);

    DECLARE product CURSOR FOR SELECT p.amount, p.code, p.id,p.remark from t_product p;

OPEN product;
while 1 = 1
        do
            FETCH product into amount,code,id,remark;
call addTestData(amount, code, id, remark);
end while;

close product;
end $$;

call createProductItemData();

INSERT INTO vaha.t_product (id, name, code, type, covert, summary, description, price, amount, specs, label, stat,
                            tenant_code, project_code, deleted, creator, create_time, updator, update_time, remark)
VALUES ('2', '北王刀', 'beiwangdao', 1,
        'http://duanju.juannai.com/upload/vod/20230916-1/9d063a9dfa278b2f74f95e55ab9f7988.jpg', '内详', '内详', '13800',
        '138', '{
    "buy": 300,
    "mode": "portrait",
    "finish": "已完结",
    "copyright": "sharing",
    "copyright-no": ""
  }', '[
    {
      "code": "new"
    },
    {
      "code": "popular"
    },
    {
      "code": "rebirth"
    }
  ]', '{
    "buy": 1335,
    "like": 20017,
    "view": 21336,
    "follow": 6283
  }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('3', '重生从离婚开始', 'zhongshengconglihunkaishi', 1,
        'http://duanju.juannai.com/upload/vod/20230701-1/2d2cc667e81496c5f47f710c9fe6cefb.jpg', '内详', '内详', '10200',
        '102', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "sweet"
         },
         {
           "code": "rebirth"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('4', '我的四个美女总裁老婆', 'wodesigemeinvzongcailaopo', 1,
        'http://duanju.juannai.com/upload/vod/20230902-1/c5e96d6c13c20918fbaf96cda6771c12.jpg', '内详', '内详', '9900',
        '99', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "comeback"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('5', '娇妻归来', 'jiaoqiguilai', 1,
        'http://duanju.juannai.com/upload/vod/20230803-1/4a667f97e8d2d159e41e406e5ef2e719.jpg', '内详', '内详', '9700',
        '97', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "sweet"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('6', '逆袭人生', 'nixirensheng', 1,
        'http://duanju.juannai.com/upload/vod/20230801-1/0ef4b4d8fb2fa2f655fbf31f2520eeda.jpg', '内详', '内详', '8000',
        '80', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "comeback"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),

       ('7', '秦爷的闪婚娇妻', 'qinyedeshanhunjiaoqi', 1,
        'http://duanju.juannai.com/upload/vod/20230801-1/64f6d97d6a704d93b1b95762089b716e.jpg', '内详', '内详', '8500',
        '85', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "sweet"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('8', '囚爱成瘾', 'qiuaichengyin', 1,
        'http://duanju.juannai.com/upload/vod/20230727-1/4d8aa8c2ca3d05838151f12076fcdbfd.png', '内详', '内详', '10000',
        '100', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "abusive"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('9', '复仇从离婚开始', 'fuchouconglihunkaishi', 1,
        'http://duanju.juannai.com/upload/vod/20230725-1/0dec00907a8f69fe5d1bf1326cb76ecc.jpg', '内详', '内详', '9400',
        '94', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "abusive"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('10', '我的绝色老板娘', 'wodejueselaobanniang', 1,
        'http://duanju.juannai.com/upload/vod/20230724-1/f435d3b493fec8e5dd2830fa9b2440e2.jpg', '内详', '内详', '7800',
        '78', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "sweet"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('11', '阎王医婿', 'yanwangyixu', 1,
        'http://duanju.juannai.com/upload/vod/20230717-1/b9abcbea40cc124b2e497c47cf582d8d.jpg', '内详', '内详', '10000',
        '100', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "rebirth"
         },
         {
           "code": "crossing"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),

       ('12', '十殿阎罗', 'shidianyanluo', 1,
        'http://duanju.juannai.com/upload/vod/20230715-1/9a281403a576dbd5a69791a402b92594.jpg', '内详', '内详', '8000',
        '80', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "abusive"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('13', '极品太子爷', 'jipintaiziye', 1,
        'http://duanju.juannai.com/upload/vod/20230704-1/3cb34df777c9f98247c500001a60a90f.jpg', '内详', '内详', '8900',
        '89', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "rebirth"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('14', '崛起从弃婚开始', 'jueqicongqihunkaishi', 1,
        'http://duanju.juannai.com/upload/vod/20230704-1/247a56b9c41007b86ef26f477bf04734.jpg', '内详', '内详', '8000',
        '80', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "comeback"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('15', '奈何少将要娶我', 'naiheshaojiangyaoquwo', 1,
        'http://duanju.juannai.com/upload/vod/20230704-1/2fb5613a5888b0003b4ca9a619cd4e26.jpg', '内详', '内详', '9300',
        '93', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "comeback"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', ''),
       ('16', '回到古代当太监', 'huidaogudaidangtaijian', 1,
        'http://duanju.juannai.com/upload/vod/20230704-1/f60d0822b1af0a5dfa37d6924197006f.jpg', '内详', '内详', '9900',
        '99', '{
         "buy": 300,
         "mode": "portrait",
         "finish": "已完结",
         "copyright": "sharing",
         "copyright-no": ""
       }', '[
         {
           "code": "new"
         },
         {
           "code": "popular"
         },
         {
           "code": "crossing"
         }
       ]', '{
         "buy": 1335,
         "like": 20017,
         "view": 21336,
         "follow": 6283
       }', 'vaha', 'vaha', 0, 'janusjia', '2023-10-14 06:42:25', 'janusjia', '2023-10-14 06:42:25', '');


truncate t_product_item;
truncate t_product;
truncate t_category_item;

update t_product_item
set detail = json_set(detail, '$."en"."720_url"',
                      concat('https://47.242.4.90/1213-十殿阎罗(80集)/第', order_num, '集.mp4')),
    detail = json_set(detail, '$."ma"."720_url"',
                      concat('https://47.242.4.90/1213-十殿阎罗(80集)/第', order_num, '集.mp4')),
    detail = json_set(detail, '$."zh"."720_url"',
                      concat('https://47.242.4.90/1213-十殿阎罗(80集)/第', order_num, '集.mp4')),
    free   = 1
where product_code = 'shidianyanluo';



INSERT INTO vaha.t_category_item (id, name, code, order_num, type, content, covert_url, link_url, tenant_code,
                                  project_code, deleted, creator, create_time, updator, update_time, remark,
                                  category_code)
VALUES ('1', '秦爷的闪婚娇妻', 'qinyedeshanhunjiaoqi', 1, 1, 'qinyedeshanhunjiaoqi',
        'http://duanju.juannai.com/upload/vod/20230801-1/64f6d97d6a704d93b1b95762089b716e.jpg', 'http://113.108.106.175:81/vaha-web/#/pages/client/video/info?id=7', 'vaha', 'vaha', 0,
        'ljm',
        null, 'ljm', null, 'remark', 'top-banner'),
       ('2', '奈何少将要娶我', 'naiheshaojiangyaoquwo', 1, 1, 'naiheshaojiangyaoquwo',
        'http://duanju.juannai.com/upload/vod/20230704-1/2fb5613a5888b0003b4ca9a619cd4e26.jpg', 'http://113.108.106.175:81/vaha-web/#/pages/client/video/info?id=15', 'vaha', 'vaha', 0,
        'ljm',
        null, 'ljm', null, 'remark', 'top-banner'),
       ('3', '阎王医婿', 'yanwangyixu', 1, 1, 'yanwangyixu',
        'http://duanju.juannai.com/upload/vod/20230717-1/b9abcbea40cc124b2e497c47cf582d8d.jpg', 'http://113.108.106.175:81/vaha-web/#/pages/client/video/info?id=11', 'vaha', 'vaha', 0,
        'ljm',
        null, 'ljm', null, 'remark', 'top-banner');
