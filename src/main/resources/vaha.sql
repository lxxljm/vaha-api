CREATE TABLE `t_category`
(
    `id`           varchar(32) NOT NULL,
    `name`         varchar(64) NOT NULL,
    `code`         varchar(64) NOT NULL,
    `description`  varchar(1024) DEFAULT NULL,
    `tenant_code`  varchar(64) NOT NULL,
    `project_code` varchar(64) NOT NULL,
    `deleted`      tinyint(1)    DEFAULT 0,
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_category_item`
(
    `id`           varchar(32)             NOT NULL,
    `name`         varchar(64)             NOT NULL,
    `code`         varchar(64)             NOT NULL,
    `order_num`    int           DEFAULT 0,
    `type`         tinyint(1)    DEFAULT 0 NOT NULL,
    `content`      varchar(1024) DEFAULT NULL,
    `covert_url`   varchar(64)   DEFAULT NULL,
    `link_url`     varchar(64)   DEFAULT NULL,
    `tenant_code`  varchar(64)             NOT NULL,
    `project_code` varchar(64)             NOT NULL,
    `deleted`      tinyint(1)    DEFAULT 0,
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_product_label`
(
    `id`           varchar(32) NOT NULL,
    `name`         varchar(64) NOT NULL,
    `code`         varchar(64) NOT NULL,
    `tenant_code`  varchar(64) NOT NULL,
    `project_code` varchar(64) NOT NULL,
    `deleted`      tinyint(1)   DEFAULT 0,
    `creator`      varchar(64)  DEFAULT '',
    `create_time`  datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)  DEFAULT '',
    `update_time`  datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_product`
(
    `id`           varchar(32)             NOT NULL,
    `name`         varchar(64)             NOT NULL,
    `code`         varchar(64)             NOT NULL,
    `type`         tinyint(1)    DEFAULT 0 NOT NULL,
    `covert`       varchar(1024) DEFAULT NULL,
    `summary`      varchar(1024) DEFAULT NULL,
    `description`  varchar(1024) DEFAULT NULL,
    `price`        varchar(32)   DEFAULT NULL,
    `amount`       varchar(32)   DEFAULT NULL,
    `specs`        json          DEFAULT NULL,
    `label`        json          DEFAULT NULL,
    `stat`         json          DEFAULT NULL,
    `tenant_code`  varchar(64)             NOT NULL,
    `project_code` varchar(64)             NOT NULL,
    `deleted`      tinyint(1)    DEFAULT 0,
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_product_item`
(
    `id`           varchar(32)             NOT NULL,
    `name`         varchar(64)             NOT NULL,
    `code`         varchar(64)             NOT NULL,
    `order_num`    int           DEFAULT 0,
    `type`         tinyint(1)    DEFAULT 0 NOT NULL,
    `currency`     tinyint(1)    DEFAULT 0 NOT NULL,
    `free`         tinyint(1)    DEFAULT 0,
    `total`        int           DEFAULT 0,
    `description`  varchar(1024) DEFAULT NULL,
    `specs`        json          DEFAULT NULL,
    `stat`         json          DEFAULT NULL,
    `detail`       json          DEFAULT NULL,
    `tenant_code`  varchar(64)             NOT NULL,
    `project_code` varchar(64)             NOT NULL,
    `deleted`      tinyint(1)    DEFAULT 0,
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_product_follow`
(
    `id`                   varchar(32) NOT NULL,
    `code`                 varchar(64) NOT NULL,
    `user_code`            varchar(64) NOT NULL,
    `product_type`         tinyint(1)   DEFAULT 0,
    `product_code`         varchar(64) NOT NULL,
    `product_item_code`    varchar(64) NOT NULL,
    `product_item_content` json         DEFAULT NULL,
    `tenant_code`          varchar(64) NOT NULL,
    `project_code`         varchar(64) NOT NULL,
    `deleted`              tinyint(1)   DEFAULT 0,
    `creator`              varchar(64)  DEFAULT '',
    `create_time`          datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`              varchar(64)  DEFAULT '',
    `update_time`          datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`               varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_order`
(
    `id`                varchar(32)            NOT NULL,
    `code`              varchar(64)            NOT NULL,
    `user_code`         varchar(64)            NOT NULL,
    `product_type`      tinyint(1)   DEFAULT 0,
    `product_code`      varchar(64)            NOT NULL,
    `product_item_code` varchar(64)            NOT NULL,
    `pay_type`          tinyint(1)   DEFAULT 0 NOT NULL,
    `transction_price`  varchar(32)  DEFAULT NULL,
    `expire_date`       varchar(32)  DEFAULT NULL,
    `product_detail`    json         DEFAULT NULL,
    `tenant_code`       varchar(64)            NOT NULL,
    `project_code`      varchar(64)            NOT NULL,
    `deleted`           tinyint(1)   DEFAULT 0,
    `creator`           varchar(64)  DEFAULT '',
    `create_time`       datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`           varchar(64)  DEFAULT '',
    `update_time`       datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`            varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user`
(
    `id`           varchar(32)             NOT NULL,
    `code`         varchar(64)             NOT NULL,
    `name`         varchar(64)             NOT NULL,
    `real_name`    varchar(64)             NOT NULL,
    `avatar`       varchar(64)             NOT NULL,
    `gender`       tinyint(1)    DEFAULT 0 NOT NULL,
    `level`        varchar(10)   DEFAULT 0 NOT NULL,
    `invite_type`  tinyint(1)    DEFAULT 0,
    `score`        varchar(64)             NOT NULL,
    `vip`          tinyint(1)    DEFAULT 0,
    `vip_exipire`  varchar(32)   DEFAULT NULL,
    `email`        varchar(64)             NOT NULL,
    `mobile`       varchar(32)   DEFAULT 0 NOT NULL,
    `birthday`     varchar(32)   DEFAULT NULL,
    `country`      varchar(10)   DEFAULT NULL,
    `region`       varchar(10)   DEFAULT NULL,
    `introduction` varchar(1024) DEFAULT NULL,
    `tenant_code`  varchar(64)             NOT NULL,
    `project_code` varchar(64)             NOT NULL,
    `deleted`      tinyint(1)    DEFAULT 0,
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user_account`
(
    `id`            varchar(32)            NOT NULL,
    `code`          varchar(64)            NOT NULL,
    `user_code`     varchar(64)            NOT NULL,
    `type`          tinyint(1)   DEFAULT 0 NOT NULL,
    `account`       varchar(32)            NOT NULL,
    `group_account` varchar(32),
    `salt`          varchar(10)            NOT NULL,
    `password`      varchar(32)            NOT NULL,
    `lock`          tinyint(1)   DEFAULT 0,
    `lock_expire`   varchar(32),
    `fail_times`    varchar(32)  DEFAULT 0 NOT NULL,
    `tenant_code`   varchar(64)            NOT NULL,
    `project_code`  varchar(64)            NOT NULL,
    `deleted`       tinyint(1)   DEFAULT 0,
    `creator`       varchar(64)  DEFAULT '',
    `create_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`       varchar(64)  DEFAULT '',
    `update_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`        varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user_login_log`
(
    `id`             varchar(32)            NOT NULL,
    `code`           varchar(64)            NOT NULL,
    `user_code`      varchar(64)            NOT NULL,
    `type`           tinyint(1)   DEFAULT 0 NOT NULL,
    `account`        varchar(32)            NOT NULL,
    `group_account`  varchar(32),
    `login_time`     varchar(32)            NOT NULL,
    `login_ip`       varchar(32)            NOT NULL,
    `login_ua`       varchar(64),
    `result`         varchar(32),
    `result_content` varchar(64)  DEFAULT 0 NOT NULL,
    `tenant_code`    varchar(64)            NOT NULL,
    `project_code`   varchar(64)            NOT NULL,
    `deleted`        tinyint(1)   DEFAULT 0,
    `creator`        varchar(64)  DEFAULT '',
    `create_time`    datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`        varchar(64)  DEFAULT '',
    `update_time`    datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`         varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user_card`
(
    `id`             varchar(32)            NOT NULL,
    `code`           varchar(64)            NOT NULL,
    `name`           varchar(64)            NOT NULL,
    `currency`       tinyint(1)   DEFAULT 0 DEFAULT NULL,
    `type`           tinyint(1)   DEFAULT 0 NOT NULL,
    `icon`           varchar(32)            NOT NULL,
    `balance`        varchar(32),
    `freeze`         tinyint(1)   DEFAULT 0 NOT NULL,
    `result_content` varchar(64)  DEFAULT 0 NOT NULL,
    `tenant_code`    varchar(64)            NOT NULL,
    `project_code`   varchar(64)            NOT NULL,
    `deleted`        tinyint(1)   DEFAULT 0,
    `creator`        varchar(64)  DEFAULT '',
    `create_time`    datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`        varchar(64)  DEFAULT '',
    `update_time`    datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`         varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user_card_bill`
(
    `id`            varchar(32)            NOT NULL,
    `card_code`     varchar(64)            NOT NULL,
    `currency`      tinyint(1)   DEFAULT 0 DEFAULT NULL,
    `type`          tinyint(1)   DEFAULT 0 NOT NULL,
    `freeze`        tinyint(1)   DEFAULT 0 NOT NULL,
    `unfreeze_date` varchar(32)            NOT NULL,
    `do`            tinyint(1)   DEFAULT 0 NOT NULL,
    `do_result`     varchar(64)            NOT NULL,
    `tenant_code`   varchar(64)            NOT NULL,
    `project_code`  varchar(64)            NOT NULL,
    `deleted`       tinyint(1)   DEFAULT 0,
    `creator`       varchar(64)  DEFAULT '',
    `create_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`       varchar(64)  DEFAULT '',
    `update_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`        varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_user_card_manifest`
(
    `id`                 varchar(32)            NOT NULL,
    `transction`         varchar(32)            NOT NULL,
    `order_code`         varchar(32)            NOT NULL,
    `from_currency`      tinyint(1)   DEFAULT 0 NOT NULL,
    `to_currency`        tinyint(1)   DEFAULT 0 NOT NULL,
    `channel`            json                   NOT NULL,
    `currency_rate`      varchar(32)  DEFAULT 0 NOT NULL,
    `transction_name`    varchar(32)            NOT NULL,
    `transction_summary` varchar(64)            NOT NULL,
    `from_amount`        varchar(32)            NOT NULL,
    `to_amount`          tinyint(32)            NOT NULL,
    `package`            tinyint(32)            NOT NULL,
    `package_content`    json                   NOT NULL,
    `tenant_code`        varchar(64)            NOT NULL,
    `project_code`       varchar(64)            NOT NULL,
    `deleted`            tinyint(1)   DEFAULT 0,
    `creator`            varchar(64)  DEFAULT '',
    `create_time`        datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`            varchar(64)  DEFAULT '',
    `update_time`        datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`             varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);



CREATE TABLE `fa_topup_package`
(
    `id`            varchar(32)            NOT NULL,
    `name`          varchar(64)            NOT NULL,
    `code`          varchar(64)            NOT NULL,
    `from_amount`   varchar(32)            NOT NULL,
    `from_currency` tinyint(1)   DEFAULT 0 NOT NULL,
    `tenant_code`   varchar(64)            NOT NULL,
    `project_code`  varchar(64)            NOT NULL,
    `deleted`       tinyint(1)   DEFAULT 0,
    `creator`       varchar(64)  DEFAULT '',
    `create_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`       varchar(64)  DEFAULT '',
    `update_time`   datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`        varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);

CREATE TABLE `t_ms_tempalte`
(
    `id`           varchar(32)               NOT NULL,
    `code`         varchar(64)               NOT NULL,
    `type`         tinyint(1)    DEFAULT '0' NOT NULL,
    `various`      json                      NOT NULL,
    `content`      varchar(1024) DEFAULT NULL,
    `tenant_code`  varchar(64)               NOT NULL,
    `project_code` varchar(64)               NOT NULL,
    `deleted`      tinyint(1)    DEFAULT '0',
    `creator`      varchar(64)   DEFAULT '',
    `create_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)   DEFAULT '',
    `update_time`  datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_ms_record`
(
    `id`            varchar(32)               NOT NULL,
    `type`          tinyint(1)    DEFAULT '0' NOT NULL,
    `send_to`       varchar(32)               NOT NULL,
    `number`        varchar(32)               NOT NULL,
    `template_code` varchar(32)               NOT NULL,
    `content`       varchar(1024) DEFAULT NULL,
    `times`         varchar(10)   DEFAULT NULL,
    `expire_time`   varchar(10)   DEFAULT NULL,
    `tenant_code`   varchar(64)               NOT NULL,
    `project_code`  varchar(64)               NOT NULL,
    `deleted`       tinyint(1)    DEFAULT '0',
    `creator`       varchar(64)   DEFAULT '',
    `create_time`   datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`       varchar(64)   DEFAULT '',
    `update_time`   datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`        varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_agent`
(
    `id`           varchar(32)              NOT NULL,
    `code`         varchar(32)              NOT NULL,
    `user_code`    varchar(32)              NOT NULL,
    `type`         tinyint(1)   DEFAULT '0' NOT NULL,
    `parent_code`  varchar(32)              NOT NULL,
    `invite_type`  tinyint(1)   DEFAULT '0' NOT NULL,
    `level`        varchar(10)  DEFAULT '0' NOT NULL,
    `country`      varchar(10)  DEFAULT NULL,
    `region`       varchar(10)  DEFAULT NULL,
    `id_number`    varchar(32)              NOT NULL,
    `id_image`     json                     NOT NULL,
    `tenant_code`  varchar(64)              NOT NULL,
    `project_code` varchar(64)              NOT NULL,
    `deleted`      tinyint(1)   DEFAULT '0',
    `creator`      varchar(64)  DEFAULT '',
    `create_time`  datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`      varchar(64)  DEFAULT '',
    `update_time`  datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`       varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_commission`
(
    `id`                      varchar(32)               NOT NULL,
    `code`                    varchar(32)               NOT NULL,
    `product_code`            varchar(32)               NOT NULL,
    `agent_code`              varchar(32)               NOT NULL,
    `commission_deep`         tinyint(1)    DEFAULT '0' NOT NULL,
    `commission_config`       varchar(64)               NOT NULL,
    `commission_introduction` varchar(1024) DEFAULT NULL,
    `tenant_code`             varchar(64)               NOT NULL,
    `project_code`            varchar(64)               NOT NULL,
    `deleted`                 tinyint(1)    DEFAULT '0',
    `creator`                 varchar(64)   DEFAULT '',
    `create_time`             datetime      DEFAULT CURRENT_TIMESTAMP,
    `updator`                 varchar(64)   DEFAULT '',
    `update_time`             datetime      DEFAULT CURRENT_TIMESTAMP,
    `remark`                  varchar(500)  DEFAULT '',
    PRIMARY KEY (`id`)
);


CREATE TABLE `t_commission_manifest`
(
    `id`                varchar(32)              NOT NULL,
    `transction`        varchar(32)              NOT NULL,
    `agent_code`        varchar(32)              NOT NULL,
    `user_transction`   varchar(32)              NOT NULL,
    `user_code`         varchar(32)              NOT NULL,
    `type`              tinyint(1)   DEFAULT '0' NOT NULL,
    `content_code`      varchar(32)              NOT NULL,
    `commission_deep`   tinyint(1)   DEFAULT '0' NOT NULL,
    `commission_rate`   varchar(64)              NOT NULL,
    `commission_amount` varchar(32)              NOT NULL,
    `tenant_code`       varchar(64)              NOT NULL,
    `project_code`      varchar(64)              NOT NULL,
    `deleted`           tinyint(1)   DEFAULT '0',
    `creator`           varchar(64)  DEFAULT '',
    `create_time`       datetime     DEFAULT CURRENT_TIMESTAMP,
    `updator`           varchar(64)  DEFAULT '',
    `update_time`       datetime     DEFAULT CURRENT_TIMESTAMP,
    `remark`            varchar(500) DEFAULT '',
    PRIMARY KEY (`id`)
);